/*
 * Active24 REST API Documentation
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * API version: 1.0.0
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package a24
import (
	"time"
)
// PaymentItemDetailData struct for PaymentItemDetailData
type PaymentItemDetailData struct {
	Currency string `json:"currency,omitempty"`
	InvoiceDocumentUrl string `json:"invoiceDocumentUrl,omitempty"`
	IssueDate time.Time `json:"issueDate,omitempty"`
	PayUrl string `json:"payUrl,omitempty"`
	PaymentRequestDocumentUrl string `json:"paymentRequestDocumentUrl,omitempty"`
	Price float64 `json:"price,omitempty"`
	Reminders []PaymentReminderData `json:"reminders,omitempty"`
	Status string `json:"status,omitempty"`
	Subject []string `json:"subject,omitempty"`
	VariableSymbol string `json:"variableSymbol,omitempty"`
}
