/*
 * Active24 REST API Documentation
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * API version: 1.0.0
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package a24
// MailboxPasswordData struct for MailboxPasswordData
type MailboxPasswordData struct {
	NewPassword string `json:"newPassword,omitempty"`
}
