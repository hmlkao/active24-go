/*
 * Active24 REST API Documentation
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * API version: 1.0.0
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package a24
// ManagedServerLicenseRestData struct for ManagedServerLicenseRestData
type ManagedServerLicenseRestData struct {
	EmailQuotaLimit int64 `json:"emailQuotaLimit,omitempty"`
	Number string `json:"number,omitempty"`
	ParameterList []LicenseParameterRestData `json:"parameterList,omitempty"`
	Servers []string `json:"servers,omitempty"`
}
