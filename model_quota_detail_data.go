/*
 * Active24 REST API Documentation
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * API version: 1.0.0
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package a24
// QuotaDetailData struct for QuotaDetailData
type QuotaDetailData struct {
	Active bool `json:"active,omitempty"`
	MinQuota int64 `json:"minQuota,omitempty"`
	Quota int64 `json:"quota,omitempty"`
	Used int64 `json:"used,omitempty"`
}
