/*
 * Active24 REST API Documentation
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * API version: 1.0.0
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package a24
// DnsRecordSrvCreate Basic DNS Record.
type DnsRecordSrvCreate struct {
	// Name of the record.
	Name string `json:"name,omitempty"`
	Port int32 `json:"port,omitempty"`
	Priority int32 `json:"priority,omitempty"`
	Target string `json:"target,omitempty"`
	// Time to live.
	Ttl int32 `json:"ttl,omitempty"`
	Weight int32 `json:"weight,omitempty"`
}
