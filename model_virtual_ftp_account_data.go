/*
 * Active24 REST API Documentation
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * API version: 1.0.0
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package a24
// VirtualFtpAccountData struct for VirtualFtpAccountData
type VirtualFtpAccountData struct {
	CountryCodes []string `json:"countryCodes,omitempty"`
	Directory string `json:"directory,omitempty"`
	HomeDir string `json:"homeDir,omitempty"`
	Ip map[string]string `json:"ip,omitempty"`
	MainAccount bool `json:"mainAccount,omitempty"`
	UserName string `json:"userName,omitempty"`
	Value string `json:"value,omitempty"`
}
