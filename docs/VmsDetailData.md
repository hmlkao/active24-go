# VmsDetailData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | [optional] 
**Ordered** | **int32** |  | [optional] 
**Os** | **string** |  | [optional] 
**PhpVersions** | **[]string** |  | [optional] 
**ServiceDataList** | [**[]ServerServiceData**](ServerServiceData.md) |  | [optional] 
**Used** | **int32** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


