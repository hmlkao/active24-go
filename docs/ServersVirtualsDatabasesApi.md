# \ServersVirtualsDatabasesApi

All URIs are relative to *http://api.active24.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddIPUsingPOST**](ServersVirtualsDatabasesApi.md#AddIPUsingPOST) | **Post** /servers/virtuals/{domain}/databases/types/{dbType}/{dbName}/allowed-ips/v1 | Adds IP address to list that is allowed to connect to database.
[**ChangeDatabasePasswordUsingPUT**](ServersVirtualsDatabasesApi.md#ChangeDatabasePasswordUsingPUT) | **Put** /servers/virtuals/{domain}/databases/types/{dbType}/{dbName}/change-password/v1 | Changes database password.
[**CreateBackupUsingPOST**](ServersVirtualsDatabasesApi.md#CreateBackupUsingPOST) | **Post** /servers/virtuals/{domain}/databases/types/{dbType}/{dbName}/backups/v1 | Creates backup of database.
[**CreateMssqlDatabaseUsingPOST**](ServersVirtualsDatabasesApi.md#CreateMssqlDatabaseUsingPOST) | **Post** /servers/virtuals/{domain}/databases/types/mssql/v1 | Creates MSSQL database.
[**CreateMysqlDatabaseUsingPOST**](ServersVirtualsDatabasesApi.md#CreateMysqlDatabaseUsingPOST) | **Post** /servers/virtuals/{domain}/databases/types/mysql/v1 | Creates MySQL database.
[**DeleteDatabaseUsingDELETE**](ServersVirtualsDatabasesApi.md#DeleteDatabaseUsingDELETE) | **Delete** /servers/virtuals/{domain}/databases/types/{dbType}/{dbName}/v1 | Deletes database.
[**DeleteIPUsingDELETE**](ServersVirtualsDatabasesApi.md#DeleteIPUsingDELETE) | **Delete** /servers/virtuals/{domain}/databases/types/{dbType}/{dbName}/allowed-ips/{ip}/v1 | Removes IP address from list that is allowed to connect to database.
[**GetAllowedIpAddressUsingGET**](ServersVirtualsDatabasesApi.md#GetAllowedIpAddressUsingGET) | **Get** /servers/virtuals/{domain}/databases/types/{dbType}/{dbName}/allowed-ips/v1 | Returns list of allowed IP adresses for connecting to the database.
[**GetDatabaseUsingGET**](ServersVirtualsDatabasesApi.md#GetDatabaseUsingGET) | **Get** /servers/virtuals/{domain}/databases/types/{dbType}/{dbName}/v1 | Returns database detail.
[**GetDatabasesListUsingGET**](ServersVirtualsDatabasesApi.md#GetDatabasesListUsingGET) | **Get** /servers/virtuals/{domain}/databases/v1 | Returns list of databases at virtual server.
[**GetProcessListUsingGET**](ServersVirtualsDatabasesApi.md#GetProcessListUsingGET) | **Get** /servers/virtuals/{domain}/databases/types/{dbType}/{dbName}/processes/v1 | Returns list of running process on database.
[**StopProcessesUsingPOST**](ServersVirtualsDatabasesApi.md#StopProcessesUsingPOST) | **Post** /servers/virtuals/{domain}/databases/types/{dbType}/{dbName}/processes/stop/v1 | Stops processes on database.



## AddIPUsingPOST

> AddIPUsingPOST(ctx, authorization, dbName, dbType, domain, form, optional)

Adds IP address to list that is allowed to connect to database.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**dbName** | **string**| dbName | 
**dbType** | **string**| dbType | 
**domain** | **string**| domain | 
**form** | [**DatabaseIp**](DatabaseIp.md)| form | 
 **optional** | ***AddIPUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a AddIPUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------





 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ChangeDatabasePasswordUsingPUT

> ChangeDatabasePasswordUsingPUT(ctx, authorization, dbName, dbType, domain, passwordChange, optional)

Changes database password.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**dbName** | **string**| dbName | 
**dbType** | **string**| dbType | 
**domain** | **string**| domain | 
**passwordChange** | [**DatabasePasswordChangeData**](DatabasePasswordChangeData.md)| passwordChange | 
 **optional** | ***ChangeDatabasePasswordUsingPUTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a ChangeDatabasePasswordUsingPUTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------





 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateBackupUsingPOST

> CreateBackupUsingPOST(ctx, authorization, dbName, dbType, domain, optional)

Creates backup of database.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**dbName** | **string**| dbName | 
**dbType** | **string**| dbType | 
**domain** | **string**| domain | 
 **optional** | ***CreateBackupUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a CreateBackupUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateMssqlDatabaseUsingPOST

> Database CreateMssqlDatabaseUsingPOST(ctx, authorization, domain, optional)

Creates MSSQL database.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
 **optional** | ***CreateMssqlDatabaseUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a CreateMssqlDatabaseUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **name** | **optional.String**|  | 

### Return type

[**Database**](Database.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateMysqlDatabaseUsingPOST

> Database CreateMysqlDatabaseUsingPOST(ctx, authorization, domain, form, optional)

Creates MySQL database.

<b>Password requirements:</b> <br>at least 10 characters<br>at least 1 Uppercase character<br>at least 1 Lowercase character<br>at least 1 number

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**form** | [**DatabaseDataCreate**](DatabaseDataCreate.md)| form | 
 **optional** | ***CreateMysqlDatabaseUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a CreateMysqlDatabaseUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

[**Database**](Database.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteDatabaseUsingDELETE

> DeleteDatabaseUsingDELETE(ctx, authorization, dbName, dbType, domain, optional)

Deletes database.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**dbName** | **string**| dbName | 
**dbType** | **string**| dbType | 
**domain** | **string**| domain | 
 **optional** | ***DeleteDatabaseUsingDELETEOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a DeleteDatabaseUsingDELETEOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteIPUsingDELETE

> DeleteIPUsingDELETE(ctx, authorization, dbName, dbType, domain, ip, optional)

Removes IP address from list that is allowed to connect to database.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**dbName** | **string**| dbName | 
**dbType** | **string**| dbType | 
**domain** | **string**| domain | 
**ip** | **string**| ip | 
 **optional** | ***DeleteIPUsingDELETEOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a DeleteIPUsingDELETEOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------





 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetAllowedIpAddressUsingGET

> []DatabaseIp GetAllowedIpAddressUsingGET(ctx, authorization, dbName, dbType, domain, optional)

Returns list of allowed IP adresses for connecting to the database.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**dbName** | **string**| dbName | 
**dbType** | **string**| dbType | 
**domain** | **string**| domain | 
 **optional** | ***GetAllowedIpAddressUsingGETOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetAllowedIpAddressUsingGETOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **name** | **optional.String**|  | 

### Return type

[**[]DatabaseIp**](DatabaseIP.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetDatabaseUsingGET

> DatabaseData GetDatabaseUsingGET(ctx, authorization, dbName, dbType, domain, optional)

Returns database detail.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**dbName** | **string**| dbName | 
**dbType** | **string**| dbType | 
**domain** | **string**| domain | 
 **optional** | ***GetDatabaseUsingGETOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetDatabaseUsingGETOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **name** | **optional.String**|  | 

### Return type

[**DatabaseData**](DatabaseData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetDatabasesListUsingGET

> []DatabaseData GetDatabasesListUsingGET(ctx, authorization, domain, optional)

Returns list of databases at virtual server.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
 **optional** | ***GetDatabasesListUsingGETOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetDatabasesListUsingGETOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **name** | **optional.String**|  | 

### Return type

[**[]DatabaseData**](DatabaseData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetProcessListUsingGET

> []DbProcessData GetProcessListUsingGET(ctx, authorization, dbName, dbType, domain, optional)

Returns list of running process on database.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**dbName** | **string**| dbName | 
**dbType** | **string**| dbType | 
**domain** | **string**| domain | 
 **optional** | ***GetProcessListUsingGETOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetProcessListUsingGETOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **name** | **optional.String**|  | 

### Return type

[**[]DbProcessData**](DBProcessData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## StopProcessesUsingPOST

> StopProcessesUsingPOST(ctx, authorization, dbName, dbType, domain, ids, optional)

Stops processes on database.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**dbName** | **string**| dbName | 
**dbType** | **string**| dbType | 
**domain** | **string**| domain | 
**ids** | [**[]int32**](int32.md)| ids | 
 **optional** | ***StopProcessesUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a StopProcessesUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------





 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

