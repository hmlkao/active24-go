# Database

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Dbname** | **string** |  | [optional] 
**Hostname** | **string** |  | [optional] 
**Login** | [**ServiceLogin**](ServiceLogin.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


