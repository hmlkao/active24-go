# ContactCzData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**City** | **string** |  | [optional] 
**ContactId** | **string** |  | [optional] 
**CountryCode** | **string** |  | [optional] 
**Disclose** | [**DomainCzContactDiscloseData**](DomainCzContactDiscloseData.md) |  | [optional] 
**Email** | **string** |  | [optional] 
**Identification** | **string** | The type of the identity document as one of values: &lt;br&gt;op (identity card number)&lt;br&gt; passport (passport number)&lt;br&gt; mpsv (number from the Ministry of Labour and Social Affairs)&lt;br&gt; ico (company number)&lt;br&gt; birthday (the date of birth) | [optional] 
**IdentificationValue** | **string** |  | [optional] 
**Name** | **string** |  | [optional] 
**NotificationEmail** | **string** |  | [optional] 
**OrganizationName** | **string** |  | [optional] 
**Phone** | **string** |  | [optional] 
**Street** | **string** |  | [optional] 
**Zip** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


