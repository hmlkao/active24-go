# DomainRegistrationData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AdminHandle** | **string** |  | [optional] 
**HolderHandle** | **string** |  | [optional] 
**KeysetHandle** | **string** |  | [optional] 
**Nameservers** | [**[]NameserverData**](NameserverData.md) |  | [optional] 
**NssetHandle** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


