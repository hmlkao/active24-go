# \ServersMultihostingApi

All URIs are relative to *http://api.active24.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateUnlimitedVirtualUsingPOST**](ServersMultihostingApi.md#CreateUnlimitedVirtualUsingPOST) | **Post** /servers/multihostings/unlimited/{name}/v1/ | Creates virtual server on Firm or Expert multihosting package.
[**DeleteUnlimitedVirtualUsingDELETE**](ServersMultihostingApi.md#DeleteUnlimitedVirtualUsingDELETE) | **Delete** /servers/multihostings/unlimited/{name}/virtuals/{domain}/v1 | Deletes virtual from Firm or Expert multihosting package.
[**GetMultihostingsUsingGET**](ServersMultihostingApi.md#GetMultihostingsUsingGET) | **Get** /servers/multihostings/v1 | Return list of all multihostings.
[**GetUnlimitedMultihostingDetailUsingGET**](ServersMultihostingApi.md#GetUnlimitedMultihostingDetailUsingGET) | **Get** /servers/multihostings/unlimited/{name}/v1 | Returns detail of Firm or Expert multihosting package.



## CreateUnlimitedVirtualUsingPOST

> CreateUnlimitedVirtualUsingPOST(ctx, authorization, name, virtualData)

Creates virtual server on Firm or Expert multihosting package.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**name** | **string**| name | 
**virtualData** | [**CreateUnlimitedVirtualServerData**](CreateUnlimitedVirtualServerData.md)| virtualData | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteUnlimitedVirtualUsingDELETE

> DeleteUnlimitedVirtualUsingDELETE(ctx, authorization, domain, name)

Deletes virtual from Firm or Expert multihosting package.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**name** | **string**| name | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetMultihostingsUsingGET

> []MultihostingSummaryData GetMultihostingsUsingGET(ctx, authorization)

Return list of all multihostings.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]

### Return type

[**[]MultihostingSummaryData**](MultihostingSummaryData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetUnlimitedMultihostingDetailUsingGET

> UnlimitedMultihostingDetailData GetUnlimitedMultihostingDetailUsingGET(ctx, authorization, name)

Returns detail of Firm or Expert multihosting package.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**name** | **string**| name | 

### Return type

[**UnlimitedMultihostingDetailData**](UnlimitedMultihostingDetailData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

