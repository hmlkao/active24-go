# EmailDomainDetailData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Aliases** | **[]string** |  | [optional] 
**Archive** | **bool** |  | [optional] 
**Catchall** | **string** |  | [optional] 
**MaxAliasCount** | **int32** |  | [optional] 
**MaxPlusUsers** | **int32** |  | [optional] 
**MaxUsers** | **int32** |  | [optional] 
**PlusUserCount** | **int32** |  | [optional] 
**Postmaster** | **string** |  | [optional] 
**ServerName** | **string** |  | [optional] 
**Users** | **[]string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


