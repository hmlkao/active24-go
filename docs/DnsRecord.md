# DnsRecord

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Alias** | **string** |  | [optional] 
**CaaValue** | **string** |  | [optional] 
**CertificateUsage** | **int32** |  | [optional] 
**Flags** | **int32** |  | [optional] 
**Hash** | **string** |  | [optional] 
**HashId** | **string** |  | [optional] 
**Ip** | **string** |  | [optional] 
**Mailserver** | **string** |  | [optional] 
**MatchingType** | **int32** |  | [optional] 
**Name** | **string** | Name of the record. | [optional] 
**Tag** | **string** |  | [optional] 
**Ttl** | **int32** | Time to live. | [optional] 
**Selector** | **int32** |  | [optional] 
**Type** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


