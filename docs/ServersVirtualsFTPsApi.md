# \ServersVirtualsFTPsApi

All URIs are relative to *http://api.active24.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ChangeFtpAccountAccessUsingPUT**](ServersVirtualsFTPsApi.md#ChangeFtpAccountAccessUsingPUT) | **Put** /servers/virtuals/{domain}/ftp-accounts/{ftpAccount}/access-restriction/v1 | Configures access restriction on FTP account.
[**ChangeFtpAccountPasswordUsingPUT**](ServersVirtualsFTPsApi.md#ChangeFtpAccountPasswordUsingPUT) | **Put** /servers/virtuals/{domain}/ftp-accounts/{ftpAccount}/change-password/v1 | Changes password on FTP account.
[**CreateFtpAccountUsingPOST**](ServersVirtualsFTPsApi.md#CreateFtpAccountUsingPOST) | **Post** /servers/virtuals/{domain}/ftp-accounts/v1 | Creates FTP account on Linux virtual server.
[**DeleteFtpAccountUsingDELETE**](ServersVirtualsFTPsApi.md#DeleteFtpAccountUsingDELETE) | **Delete** /servers/virtuals/{domain}/ftp-accounts/{ftpAccount}/v1 | Deletes FTP account.
[**GetFtpAccountAccessRestrictionUsingGET**](ServersVirtualsFTPsApi.md#GetFtpAccountAccessRestrictionUsingGET) | **Get** /servers/virtuals/{domain}/ftp-accounts/{ftpAccount}/access-restriction/v1 | Returns access restriction configuration on FTP account.
[**GetFtpAccountsInfoUsingGET**](ServersVirtualsFTPsApi.md#GetFtpAccountsInfoUsingGET) | **Get** /servers/virtuals/{domain}/ftp-accounts/v1 | Returns list of FTP accounts on virtual server.



## ChangeFtpAccountAccessUsingPUT

> ChangeFtpAccountAccessUsingPUT(ctx, authorization, domain, ftpAccount, accessData)

Configures access restriction on FTP account.

Attributes allowedCountryCodeList and allowedIpList are used only for custom restriction type.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**ftpAccount** | **string**| ftpAccount | 
**accessData** | [**FtpAccountAccessRestrictionInputData**](FtpAccountAccessRestrictionInputData.md)| accessData | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ChangeFtpAccountPasswordUsingPUT

> ChangeFtpAccountPasswordUsingPUT(ctx, authorization, domain, ftpAccount, passwordChange)

Changes password on FTP account.

<b>Password requirements:</b> <br>at least 10 characters<br>at least 1 Uppercase character<br>at least 1 Lowercase character<br>at least 1 number

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**ftpAccount** | **string**| ftpAccount | 
**passwordChange** | [**FtpPasswordChangeData**](FtpPasswordChangeData.md)| passwordChange | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateFtpAccountUsingPOST

> CreateFtpAccountResult CreateFtpAccountUsingPOST(ctx, authorization, domain, createFtpAccountData)

Creates FTP account on Linux virtual server.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**createFtpAccountData** | [**CreateFtpAccountData**](CreateFtpAccountData.md)| createFtpAccountData | 

### Return type

[**CreateFtpAccountResult**](CreateFtpAccountResult.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteFtpAccountUsingDELETE

> DeleteFtpAccountUsingDELETE(ctx, authorization, domain, ftpAccount)

Deletes FTP account.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**ftpAccount** | **string**| ftpAccount | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetFtpAccountAccessRestrictionUsingGET

> FtpAccountAccessRestrictionData GetFtpAccountAccessRestrictionUsingGET(ctx, authorization, domain, ftpAccount)

Returns access restriction configuration on FTP account.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**ftpAccount** | **string**| ftpAccount | 

### Return type

[**FtpAccountAccessRestrictionData**](FtpAccountAccessRestrictionData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetFtpAccountsInfoUsingGET

> VirtualFtpData GetFtpAccountsInfoUsingGET(ctx, authorization, domain)

Returns list of FTP accounts on virtual server.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 

### Return type

[**VirtualFtpData**](VirtualFtpData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

