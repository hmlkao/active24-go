# VirtualFtpData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AlternativeHostUrl** | **string** |  | [optional] 
**HostUrl** | **string** |  | [optional] 
**MaxAccounts** | **int32** |  | [optional] 
**VirtualFtpAccountDataList** | [**[]VirtualFtpAccountData**](VirtualFtpAccountData.md) |  | [optional] 
**WinFtpUsers** | [**[]WinFtpUserFrontendData**](WinFtpUserFrontendData.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


