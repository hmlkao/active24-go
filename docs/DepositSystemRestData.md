# DepositSystemRestData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DepositAmout** | **float32** |  | [optional] 
**Deposits** | [**[]DepositSystemData**](DepositSystemData.md) |  | [optional] 
**Payments** | [**[]DepositSystemPaymentData**](DepositSystemPaymentData.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


