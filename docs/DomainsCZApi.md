# \DomainsCZApi

All URIs are relative to *http://api.active24.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetDomainDetailUsingGET**](DomainsCZApi.md#GetDomainDetailUsingGET) | **Get** /domains/cz/{domain}/v1 | Returns the detail of a domain.
[**RegisterContactCzUsingPOST**](DomainsCZApi.md#RegisterContactCzUsingPOST) | **Post** /domains/cz/contact/v1 | Creates new CZ-NIC contact.
[**TransferDomainUsingPOST**](DomainsCZApi.md#TransferDomainUsingPOST) | **Post** /domains/cz/transfer-domain/v1 | Transfers a .CZ domain to ACTIVE 24 from other registrar.
[**TransferDomainWithNssetUsingPOST**](DomainsCZApi.md#TransferDomainWithNssetUsingPOST) | **Post** /domains/cz/transfer-domain/v2 | Transfers a .CZ domain to ACTIVE 24 from other registrar. Can also transfer nameserver records.
[**UpdateDomainUsingPUT**](DomainsCZApi.md#UpdateDomainUsingPUT) | **Put** /domains/cz/{domain}/v1 | Changes domain objects of a .CZ domain



## GetDomainDetailUsingGET

> DomainCzDetailData GetDomainDetailUsingGET(ctx, authorization, domain, optional)

Returns the detail of a domain.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
 **optional** | ***GetDomainDetailUsingGETOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetDomainDetailUsingGETOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **name** | **optional.String**|  | 

### Return type

[**DomainCzDetailData**](DomainCzDetailData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## RegisterContactCzUsingPOST

> RegisterContactCzUsingPOST(ctx, authorization, data, optional)

Creates new CZ-NIC contact.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**data** | [**ContactCzData**](ContactCzData.md)| data | 
 **optional** | ***RegisterContactCzUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a RegisterContactCzUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## TransferDomainUsingPOST

> TransferDomainUsingPOST(ctx, authorization, transferDomain, optional)

Transfers a .CZ domain to ACTIVE 24 from other registrar.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**transferDomain** | [**TransferDomainData**](TransferDomainData.md)| transferDomain | 
 **optional** | ***TransferDomainUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a TransferDomainUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## TransferDomainWithNssetUsingPOST

> TransferDomainWithNssetUsingPOST(ctx, authorization, transferDomain, optional)

Transfers a .CZ domain to ACTIVE 24 from other registrar. Can also transfer nameserver records.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**transferDomain** | [**TransferDomainDataV2**](TransferDomainDataV2.md)| transferDomain | 
 **optional** | ***TransferDomainWithNssetUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a TransferDomainWithNssetUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateDomainUsingPUT

> UpdateDomainUsingPUT(ctx, authorization, domain, domainCzUpdateData, optional)

Changes domain objects of a .CZ domain

If some attribute has not been filled, null value will be set in central registry - current value will be overwritten.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**domainCzUpdateData** | [**DomainCzUpdateData**](DomainCzUpdateData.md)| domainCzUpdateData | 
 **optional** | ***UpdateDomainUsingPUTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a UpdateDomainUsingPUTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

