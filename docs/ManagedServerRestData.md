# ManagedServerRestData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**LicenseNumber** | **string** |  | [optional] 
**Os** | **string** |  | [optional] 
**ServerName** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


