# DomainRegistrationPaymentBankTransferData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BankAccount** | **string** |  | [optional] 
**BankCode** | **string** |  | [optional] 
**Currency** | **string** |  | [optional] 
**Iban** | **string** |  | [optional] 
**Price** | **float32** |  | [optional] 
**QrCode** | **string** |  | [optional] 
**Swift** | **string** |  | [optional] 
**VariableSymbol** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


