# DomainCzDetailData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**A24Registrator** | **bool** |  | [optional] 
**AdminContact** | **[]string** |  | [optional] 
**Created** | [**time.Time**](time.Time.md) |  | [optional] 
**Creator** | **string** |  | [optional] 
**Domain** | **string** |  | [optional] 
**Expiration** | [**time.Time**](time.Time.md) |  | [optional] 
**HolderContact** | **string** |  | [optional] 
**Keyset** | **string** |  | [optional] 
**LastChangeAuthor** | **string** |  | [optional] 
**LastChangeDate** | [**time.Time**](time.Time.md) |  | [optional] 
**LastTransfer** | [**time.Time**](time.Time.md) |  | [optional] 
**NicId** | **string** |  | [optional] 
**Nsset** | **string** |  | [optional] 
**PeriodNum** | **int32** |  | [optional] 
**PeriodType** | **string** |  | [optional] 
**Registrar** | **string** |  | [optional] 
**Status** | **string** |  | [optional] 
**TemporaryContact** | **[]string** |  | [optional] 
**Validity** | [**time.Time**](time.Time.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


