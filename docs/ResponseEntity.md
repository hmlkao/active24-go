# ResponseEntity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Body** | [**map[string]interface{}**](.md) |  | [optional] 
**StatusCode** | **string** |  | [optional] 
**StatusCodeValue** | **int32** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


