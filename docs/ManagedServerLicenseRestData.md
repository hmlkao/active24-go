# ManagedServerLicenseRestData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EmailQuotaLimit** | **int64** |  | [optional] 
**Number** | **string** |  | [optional] 
**ParameterList** | [**[]LicenseParameterRestData**](LicenseParameterRestData.md) |  | [optional] 
**Servers** | **[]string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


