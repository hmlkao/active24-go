# TransferDomainDataV2

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AuthInfo** | **string** |  | [optional] 
**DomainName** | **string** |  | [optional] 
**SetA24Nameservers** | **bool** | Specifies if nameservers should be changed to Active24 ones. Changes domain NSSET a Keyset to Active24. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


