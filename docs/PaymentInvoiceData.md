# PaymentInvoiceData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CreditNote** | **bool** |  | [optional] 
**DocumentUrl** | **string** |  | [optional] 
**InvoiceIdentifier** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


