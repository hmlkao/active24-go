# \ServersVMSApi

All URIs are relative to *http://api.active24.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateVmsLinuxVirtualUsingPOST**](ServersVMSApi.md#CreateVmsLinuxVirtualUsingPOST) | **Post** /servers/vms/linux/{serverName}/virtuals/v1 | Creates virtual on Linux VMS.
[**CreateVmsWindowsVirtualUsingPOST**](ServersVMSApi.md#CreateVmsWindowsVirtualUsingPOST) | **Post** /servers/vms/windows/{serverName}/virtuals/v1 | Creates virtual on Windows VMS.
[**DeleteVmsVirtualUsingDELETE**](ServersVMSApi.md#DeleteVmsVirtualUsingDELETE) | **Delete** /servers/vms/{serverName}/virtuals/{domain}/v1 | Deletes VMS virtual server.
[**GetQuotaListUsingGET1**](ServersVMSApi.md#GetQuotaListUsingGET1) | **Get** /servers/vms/{vmsName}/quotas/v1 | Returns quotas for VMS virtual servers.
[**GetServerDetailUsingGET1**](ServersVMSApi.md#GetServerDetailUsingGET1) | **Get** /servers/vms/{vmsName}/v1 | Returns VMS detail.
[**SetQuotaDataUsingPUT1**](ServersVMSApi.md#SetQuotaDataUsingPUT1) | **Put** /servers/vms/{serverName}/virtuals/{domain}/quotas/data/v1 | Sets new space quota for virtual server.
[**SetQuotaEmailUsingPUT1**](ServersVMSApi.md#SetQuotaEmailUsingPUT1) | **Put** /servers/vms/{serverName}/virtuals/{domain}/quotas/email/v1 | Sets new quota for email service.



## CreateVmsLinuxVirtualUsingPOST

> CreateVmsLinuxVirtualUsingPOST(ctx, authorization, serverName, data, optional)

Creates virtual on Linux VMS.

Available PHP versions could be found by calling method for getting server detail.<br>Attribute mailSpace is not required, during creating space only for web presentation.<br>Attribute diskSpace and phpVersion are not required, during creating mailspace only.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**serverName** | **string**| serverName | 
**data** | [**CreateVmsLinuxVirtualParams**](CreateVmsLinuxVirtualParams.md)| data | 
 **optional** | ***CreateVmsLinuxVirtualUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a CreateVmsLinuxVirtualUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateVmsWindowsVirtualUsingPOST

> CreateVmsWindowsVirtualUsingPOST(ctx, authorization, serverName, data, optional)

Creates virtual on Windows VMS.

Attribute mailSpace is not required, during creating space only for web presentation.<br>Attribute diskSpace and databaseType are not required, during creating mailspace only.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**serverName** | **string**| serverName | 
**data** | [**CreateVmsWindowsVirtualParams**](CreateVmsWindowsVirtualParams.md)| data | 
 **optional** | ***CreateVmsWindowsVirtualUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a CreateVmsWindowsVirtualUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteVmsVirtualUsingDELETE

> DeleteVmsVirtualUsingDELETE(ctx, authorization, domain, serverName, optional)

Deletes VMS virtual server.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**serverName** | **string**| serverName | 
 **optional** | ***DeleteVmsVirtualUsingDELETEOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a DeleteVmsVirtualUsingDELETEOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetQuotaListUsingGET1

> []ServerDomainQuotaData GetQuotaListUsingGET1(ctx, authorization, vmsName)

Returns quotas for VMS virtual servers.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**vmsName** | **string**| vmsName | 

### Return type

[**[]ServerDomainQuotaData**](ServerDomainQuotaData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetServerDetailUsingGET1

> VmsDetailData GetServerDetailUsingGET1(ctx, authorization, vmsName, optional)

Returns VMS detail.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**vmsName** | **string**| vmsName | 
 **optional** | ***GetServerDetailUsingGET1Opts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetServerDetailUsingGET1Opts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **name** | **optional.String**|  | 

### Return type

[**VmsDetailData**](VmsDetailData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SetQuotaDataUsingPUT1

> SetQuotaDataUsingPUT1(ctx, authorization, domain, serverName, quota)

Sets new space quota for virtual server.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**serverName** | **string**| serverName | 
**quota** | [**MailboxQuotaData**](MailboxQuotaData.md)| quota | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SetQuotaEmailUsingPUT1

> SetQuotaEmailUsingPUT1(ctx, authorization, domain, serverName, quota, optional)

Sets new quota for email service.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**serverName** | **string**| serverName | 
**quota** | [**MailboxQuotaData**](MailboxQuotaData.md)| quota | 
 **optional** | ***SetQuotaEmailUsingPUT1Opts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a SetQuotaEmailUsingPUT1Opts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

