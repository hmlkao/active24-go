# \DomainsApi

All URIs are relative to *http://api.active24.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetDomainExpirationUsingGET**](DomainsApi.md#GetDomainExpirationUsingGET) | **Get** /domains/{domainName}/expiration/v1 | Gets domain expiration date.
[**GetDomainStatusUsingGET**](DomainsApi.md#GetDomainStatusUsingGET) | **Get** /domains/{domainName}/status/v1 | Get domain status - checks if domain can be registered.
[**GetDomainsUsingGET**](DomainsApi.md#GetDomainsUsingGET) | **Get** /domains/v1 | Returns a list of domains where user is owner or payer.
[**RegisterDomainUsingPOST**](DomainsApi.md#RegisterDomainUsingPOST) | **Post** /domains/{domainName}/register/v1 | Registers new domain.
[**SendAuthInfoUsingPOST**](DomainsApi.md#SendAuthInfoUsingPOST) | **Post** /domains/{domainName}/authInfo/{language}/v1 | Sends an auth info to the owner of domain for authorization of changes.



## GetDomainExpirationUsingGET

> time.Time GetDomainExpirationUsingGET(ctx, authorization, domainName, optional)

Gets domain expiration date.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domainName** | **string**| domainName | 
 **optional** | ***GetDomainExpirationUsingGETOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetDomainExpirationUsingGETOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **name** | **optional.String**|  | 

### Return type

[**time.Time**](time.Time.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetDomainStatusUsingGET

> string GetDomainStatusUsingGET(ctx, authorization, domainName, optional)

Get domain status - checks if domain can be registered.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domainName** | **string**| domainName | 
 **optional** | ***GetDomainStatusUsingGETOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetDomainStatusUsingGETOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **name** | **optional.String**|  | 

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetDomainsUsingGET

> []DomainData GetDomainsUsingGET(ctx, authorization, optional)

Returns a list of domains where user is owner or payer.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
 **optional** | ***GetDomainsUsingGETOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetDomainsUsingGETOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **name** | **optional.String**|  | 

### Return type

[**[]DomainData**](DomainData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## RegisterDomainUsingPOST

> DomainRegistrationPaymentDetailData RegisterDomainUsingPOST(ctx, authorization, domainName, input, optional)

Registers new domain.

<b>Supported TLDs:</b> <br>CZ<br>SK<br>EU<br>COM<br>NET<br>BIZ<br>ORG<br>INFO

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domainName** | **string**| domainName | 
**input** | [**DomainRegistrationData**](DomainRegistrationData.md)| input | 
 **optional** | ***RegisterDomainUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a RegisterDomainUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

[**DomainRegistrationPaymentDetailData**](DomainRegistrationPaymentDetailData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SendAuthInfoUsingPOST

> SendAuthInfoUsingPOST(ctx, authorization, domainName, language, optional)

Sends an auth info to the owner of domain for authorization of changes.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domainName** | **string**| domainName | 
**language** | **string**| cs, sk, en, de, nl, es, gb, ca | 
 **optional** | ***SendAuthInfoUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a SendAuthInfoUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

