# A24EmailBox

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Aliases** | **[]string** |  | [optional] 
**Quota** | **int64** |  | [optional] 
**UsedSpace** | **int64** |  | [optional] 
**User** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


