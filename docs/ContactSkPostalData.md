# ContactSkPostalData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Address** | [**ContactSkAddressData**](ContactSkAddressData.md) |  | [optional] 
**Name** | **string** |  | [optional] 
**Organization** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


