# DepositSystemPaymentData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Created** | [**time.Time**](time.Time.md) |  | [optional] 
**Domain** | **string** |  | [optional] 
**From** | [**time.Time**](time.Time.md) |  | [optional] 
**Price** | **float32** |  | [optional] 
**PriceVatIncluded** | **float32** |  | [optional] 
**To** | [**time.Time**](time.Time.md) |  | [optional] 
**Type** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


