# \ServersVirtualsApi

All URIs are relative to *http://api.active24.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GeAspVersionsUsingGET**](ServersVirtualsApi.md#GeAspVersionsUsingGET) | **Get** /servers/virtuals/windows/{domain}/asp/version/v1 | Returns a list of available ASP versions and current ASP version on virtual server.
[**GetPhpVersionsUsingGET1**](ServersVirtualsApi.md#GetPhpVersionsUsingGET1) | **Get** /servers/virtuals/linux/{domain}/php/version/v1 | Returns a list of available PHP versions and current PHP version on virtual server.
[**GetQuotaDataUsingGET**](ServersVirtualsApi.md#GetQuotaDataUsingGET) | **Get** /servers/virtuals/linux/{domain}/quota/v1 | Returns virtual server quotas.
[**GetVirtualServerDetailUsingGET**](ServersVirtualsApi.md#GetVirtualServerDetailUsingGET) | **Get** /servers/virtuals/{domain}/v1 | Returns detail of virtual server.
[**GetVirtualServersListUsingGET**](ServersVirtualsApi.md#GetVirtualServersListUsingGET) | **Get** /servers/virtuals/v1 | Returns list of virtual servers.
[**SetAspVersionUsingPUT**](ServersVirtualsApi.md#SetAspVersionUsingPUT) | **Put** /servers/virtuals/windows/{domain}/asp/versions/v1 | Sets ASP version on virtual server.
[**SetPhpVersionUsingPUT**](ServersVirtualsApi.md#SetPhpVersionUsingPUT) | **Put** /servers/virtuals/linux/{domain}/php/version/v1 | Sets PHP version on virtual server.



## GeAspVersionsUsingGET

> AspData GeAspVersionsUsingGET(ctx, authorization, domain, optional)

Returns a list of available ASP versions and current ASP version on virtual server.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
 **optional** | ***GeAspVersionsUsingGETOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GeAspVersionsUsingGETOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **name** | **optional.String**|  | 

### Return type

[**AspData**](AspData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetPhpVersionsUsingGET1

> PhpData GetPhpVersionsUsingGET1(ctx, authorization, domain, optional)

Returns a list of available PHP versions and current PHP version on virtual server.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
 **optional** | ***GetPhpVersionsUsingGET1Opts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetPhpVersionsUsingGET1Opts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **name** | **optional.String**|  | 

### Return type

[**PhpData**](PhpData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetQuotaDataUsingGET

> QuotaData GetQuotaDataUsingGET(ctx, authorization, domain, optional)

Returns virtual server quotas.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
 **optional** | ***GetQuotaDataUsingGETOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetQuotaDataUsingGETOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **name** | **optional.String**|  | 

### Return type

[**QuotaData**](QuotaData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetVirtualServerDetailUsingGET

> VirtualServerDetailData GetVirtualServerDetailUsingGET(ctx, authorization, domain, optional)

Returns detail of virtual server.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
 **optional** | ***GetVirtualServerDetailUsingGETOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetVirtualServerDetailUsingGETOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **name** | **optional.String**|  | 

### Return type

[**VirtualServerDetailData**](VirtualServerDetailData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetVirtualServersListUsingGET

> []VirtualServerData GetVirtualServersListUsingGET(ctx, authorization, optional)

Returns list of virtual servers.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
 **optional** | ***GetVirtualServersListUsingGETOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetVirtualServersListUsingGETOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **name** | **optional.String**|  | 

### Return type

[**[]VirtualServerData**](VirtualServerData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SetAspVersionUsingPUT

> SetAspVersionUsingPUT(ctx, authorization, domain, version)

Sets ASP version on virtual server.

Available ASP versions could be found by calling method for getting ASP versions.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**version** | [**AspVersionData**](AspVersionData.md)| version | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SetPhpVersionUsingPUT

> SetPhpVersionUsingPUT(ctx, authorization, domain, version, optional)

Sets PHP version on virtual server.

Available PHP versions could be found by calling method for getting PHP versions.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**version** | [**PhpVersionData**](PhpVersionData.md)| version | 
 **optional** | ***SetPhpVersionUsingPUTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a SetPhpVersionUsingPUTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

