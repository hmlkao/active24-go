# PaymentReminderData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AddressList** | **[]string** |  | [optional] 
**DateOfPost** | [**time.Time**](time.Time.md) |  | [optional] 
**Order** | **int32** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


