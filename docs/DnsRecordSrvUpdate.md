# DnsRecordSrvUpdate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**HashId** | **string** |  | [optional] 
**Name** | **string** | Name of the record. | [optional] 
**Port** | **int32** |  | [optional] 
**Priority** | **int32** |  | [optional] 
**Target** | **string** |  | [optional] 
**Ttl** | **int32** | Time to live. | [optional] 
**Weight** | **int32** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


