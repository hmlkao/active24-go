# \DomainsOtherThanCZAndSKApi

All URIs are relative to *http://api.active24.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetDomainUsingGET**](DomainsOtherThanCZAndSKApi.md#GetDomainUsingGET) | **Get** /domains/other/{domainName}/v1 | Returns domain configuration.



## GetDomainUsingGET

> AutomatedDomainConfigurationData GetDomainUsingGET(ctx, authorization, domainName, optional)

Returns domain configuration.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domainName** | **string**| domainName | 
 **optional** | ***GetDomainUsingGETOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetDomainUsingGETOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **name** | **optional.String**|  | 

### Return type

[**AutomatedDomainConfigurationData**](AutomatedDomainConfigurationData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

