# DnsRecordTlsaCreate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CertificateUsage** | **int32** |  | [optional] 
**Hash** | **string** |  | [optional] 
**MatchingType** | **int32** |  | [optional] 
**Name** | **string** | Name of the record. | [optional] 
**Selector** | **int32** |  | [optional] 
**Ttl** | **int32** | Time to live. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


