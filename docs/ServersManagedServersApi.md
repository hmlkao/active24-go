# \ServersManagedServersApi

All URIs are relative to *http://api.active24.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateMsLinuxVirtualUsingPOST**](ServersManagedServersApi.md#CreateMsLinuxVirtualUsingPOST) | **Post** /servers/ms/linux/{serverName}/virtuals/v1 | Creates virtual server on Linux managed server.
[**CreateMsWindowsVirtualUsingPOST**](ServersManagedServersApi.md#CreateMsWindowsVirtualUsingPOST) | **Post** /servers/ms/windows/{serverName}/virtuals/v1 | Creates virtual server on Windows managed server.
[**DeleteMsVirtualUsingDELETE**](ServersManagedServersApi.md#DeleteMsVirtualUsingDELETE) | **Delete** /servers/ms/{serverName}/virtuals/{domain}/v1 | Deletes virtual server from managed server.
[**GetManagedServersUsingGET**](ServersManagedServersApi.md#GetManagedServersUsingGET) | **Get** /servers/ms/v1 | Returns list of managed servers.
[**GetPhpVersionsUsingGET**](ServersManagedServersApi.md#GetPhpVersionsUsingGET) | **Get** /servers/ms/{serverName}/php-versions/v1 | Returns list of available PHP versions.
[**GetQuotaListUsingGET**](ServersManagedServersApi.md#GetQuotaListUsingGET) | **Get** /servers/ms/{serverName}/quotas/v1 | Returns list of virtual servers with their quotas.
[**GetServerDetailUsingGET**](ServersManagedServersApi.md#GetServerDetailUsingGET) | **Get** /servers/ms/{serverName}/v1 | Returns detail of managed server.
[**SetQuotaDataUsingPUT**](ServersManagedServersApi.md#SetQuotaDataUsingPUT) | **Put** /servers/ms/quotas/{domain}/data/v1 | Sets new space quota for virtual server.
[**SetQuotaEmailUsingPUT**](ServersManagedServersApi.md#SetQuotaEmailUsingPUT) | **Put** /servers/ms/quotas/{domain}/email/v1 | Sets new space quota for email service.



## CreateMsLinuxVirtualUsingPOST

> CreateMsLinuxVirtualUsingPOST(ctx, authorization, serverName, createMsLinuxVirtualParams, optional)

Creates virtual server on Linux managed server.

Available PHP versions could be found by calling method for getting server detail.<br>Attribute mailSpace is not required, during creating space only for web presentation.<br>Attribute diskSpace and phpVersion are not required, during creating mailspace only.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**serverName** | **string**| serverName | 
**createMsLinuxVirtualParams** | [**CreateMsLinuxVirtualParams**](CreateMsLinuxVirtualParams.md)| createMsLinuxVirtualParams | 
 **optional** | ***CreateMsLinuxVirtualUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a CreateMsLinuxVirtualUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateMsWindowsVirtualUsingPOST

> CreateMsWindowsVirtualUsingPOST(ctx, authorization, serverName, createMsWindowsVirtualParams, optional)

Creates virtual server on Windows managed server.

Attribute mailSpace is not required, during creating space only for web presentation.<br>Attribute diskSpace and databaseType are not required, during creating mailspace only.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**serverName** | **string**| serverName | 
**createMsWindowsVirtualParams** | [**CreateMsWindowsVirtualParams**](CreateMsWindowsVirtualParams.md)| createMsWindowsVirtualParams | 
 **optional** | ***CreateMsWindowsVirtualUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a CreateMsWindowsVirtualUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteMsVirtualUsingDELETE

> DeleteMsVirtualUsingDELETE(ctx, authorization, domain, serverName, optional)

Deletes virtual server from managed server.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**serverName** | **string**| serverName | 
 **optional** | ***DeleteMsVirtualUsingDELETEOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a DeleteMsVirtualUsingDELETEOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetManagedServersUsingGET

> []ManagedServerRestData GetManagedServersUsingGET(ctx, authorization)

Returns list of managed servers.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]

### Return type

[**[]ManagedServerRestData**](ManagedServerRestData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetPhpVersionsUsingGET

> []ScriptVersion GetPhpVersionsUsingGET(ctx, authorization, serverName)

Returns list of available PHP versions.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**serverName** | **string**| serverName | 

### Return type

[**[]ScriptVersion**](ScriptVersion.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetQuotaListUsingGET

> []ServerDomainQuotaData GetQuotaListUsingGET(ctx, authorization, serverName)

Returns list of virtual servers with their quotas.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**serverName** | **string**| serverName | 

### Return type

[**[]ServerDomainQuotaData**](ServerDomainQuotaData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetServerDetailUsingGET

> ManagedServerDetailRestData GetServerDetailUsingGET(ctx, authorization, serverName, optional)

Returns detail of managed server.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**serverName** | **string**| serverName | 
 **optional** | ***GetServerDetailUsingGETOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetServerDetailUsingGETOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **name** | **optional.String**|  | 

### Return type

[**ManagedServerDetailRestData**](ManagedServerDetailRestData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SetQuotaDataUsingPUT

> SetQuotaDataUsingPUT(ctx, authorization, domain, newQuota)

Sets new space quota for virtual server.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**newQuota** | [**QuotaRequest**](QuotaRequest.md)| newQuota | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SetQuotaEmailUsingPUT

> SetQuotaEmailUsingPUT(ctx, authorization, domain, newQuota, optional)

Sets new space quota for email service.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**newQuota** | [**QuotaRequest**](QuotaRequest.md)| newQuota | 
 **optional** | ***SetQuotaEmailUsingPUTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a SetQuotaEmailUsingPUTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

