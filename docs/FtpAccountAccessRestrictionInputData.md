# FtpAccountAccessRestrictionInputData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AllowedCountryCodeList** | **[]string** |  | [optional] 
**AllowedIpList** | [**[]IpAddressDetailData**](IpAddressDetailData.md) |  | [optional] 
**Type** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


