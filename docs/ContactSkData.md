# ContactSkData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Disclose** | [**ContactSkDiscloseData**](ContactSkDiscloseData.md) |  | [optional] 
**Email** | **string** |  | [optional] 
**Fax** | **string** |  | [optional] 
**Id** | **string** |  | [optional] 
**IdentificationType** | **string** |  | [optional] 
**IdentificationValue** | **string** |  | [optional] 
**Phone** | **string** |  | [optional] 
**PostalInfo** | [**ContactSkPostalData**](ContactSkPostalData.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


