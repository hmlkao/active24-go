# AccountInfoData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Aliases** | **[]string** |  | [optional] 
**Antispam** | **bool** |  | [optional] 
**Autoreply** | [**AccountAutoreplyData**](AccountAutoreplyData.md) |  | [optional] 
**AutoreplyActivateable** | **bool** |  | [optional] 
**AvailableQuota** | **int64** |  | [optional] 
**AvailableQuotaCapacity** | **int64** |  | [optional] 
**AvailableQuotaSize** | **int64** |  | [optional] 
**Delivery** | **bool** |  | [optional] 
**Domain** | **string** |  | [optional] 
**Filter** | [**map[string]interface{}**](.md) |  | [optional] 
**FiltersHideable** | **bool** |  | [optional] 
**Forward** | [**[]IdnPunyEmail**](IdnPunyEmail.md) |  | [optional] 
**FreeSpace** | **int64** |  | [optional] 
**Full** | **bool** |  | [optional] 
**GreylistStatus** | **bool** |  | [optional] 
**ImapFolderNamespace** | **string** |  | [optional] 
**MailboxDeleteable** | **bool** |  | [optional] 
**MailboxTypes** | **[]string** |  | [optional] 
**Maildirs** | [**[]map[string]map[string]interface{}**](map.md) |  | [optional] 
**MaxQuotaExceeded** | **bool** |  | [optional] 
**Notify** | [**[]IdnPunyEmail**](IdnPunyEmail.md) |  | [optional] 
**Password** | **bool** |  | [optional] 
**Plus** | **bool** |  | [optional] 
**Quota** | **int64** |  | [optional] 
**QuotaChangeable** | **bool** |  | [optional] 
**Settings** | [**AccountSettingsData**](AccountSettingsData.md) |  | [optional] 
**Space** | **int64** |  | [optional] 
**UsedSpace** | **int64** |  | [optional] 
**User** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


