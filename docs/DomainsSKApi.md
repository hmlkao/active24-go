# \DomainsSKApi

All URIs are relative to *http://api.active24.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ChangeNameserversSkUsingPUT**](DomainsSKApi.md#ChangeNameserversSkUsingPUT) | **Put** /domains/sk/{domainName}/nameservers/v1 | Updates nameservers.
[**GetDomainDetailUsingGET1**](DomainsSKApi.md#GetDomainDetailUsingGET1) | **Get** /domains/sk/{domain}/v1 | Returns domain detail.
[**RegisterContactUsingPOST**](DomainsSKApi.md#RegisterContactUsingPOST) | **Post** /domains/sk/contact/v1 | Creates new SK-NIC contact.



## ChangeNameserversSkUsingPUT

> ChangeNameserversSkUsingPUT(ctx, authorization, domainName, nameservers, optional)

Updates nameservers.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domainName** | **string**| domainName | 
**nameservers** | [**[]string**](string.md)| nameservers | 
 **optional** | ***ChangeNameserversSkUsingPUTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a ChangeNameserversSkUsingPUTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetDomainDetailUsingGET1

> DomainSkDetailData GetDomainDetailUsingGET1(ctx, authorization, domain, optional)

Returns domain detail.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
 **optional** | ***GetDomainDetailUsingGET1Opts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetDomainDetailUsingGET1Opts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **name** | **optional.String**|  | 

### Return type

[**DomainSkDetailData**](DomainSkDetailData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## RegisterContactUsingPOST

> RegisterContactUsingPOST(ctx, authorization, data, optional)

Creates new SK-NIC contact.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**data** | [**ContactSkData**](ContactSkData.md)| data | 
 **optional** | ***RegisterContactUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a RegisterContactUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

