# DepositSystemFuturePaymentData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AllowStorno** | **bool** |  | [optional] 
**Domain** | **string** |  | [optional] 
**FromDate** | [**time.Time**](time.Time.md) |  | 
**InvoiceId** | **int32** |  | [optional] 
**OrderId** | **int32** |  | [optional] 
**Price** | **float32** |  | [optional] 
**PriceVatInc** | **float32** |  | [optional] 
**StornoDate** | [**time.Time**](time.Time.md) |  | 
**ToDate** | [**time.Time**](time.Time.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


