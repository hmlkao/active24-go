# DomainSkDetailData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AdminContactId** | **string** |  | [optional] 
**BillingContactId** | **string** |  | [optional] 
**CurrentRegistrar** | **string** |  | [optional] 
**DomainName** | **string** |  | [optional] 
**ExpirationDate** | [**time.Time**](time.Time.md) |  | [optional] 
**HolderId** | **string** |  | [optional] 
**LastUpdateDate** | [**time.Time**](time.Time.md) |  | [optional] 
**LastUpdateRegistrar** | **string** |  | [optional] 
**Nameservers** | **[]string** |  | [optional] 
**OriginalRegistrar** | **string** |  | [optional] 
**RegistrationDate** | [**time.Time**](time.Time.md) |  | [optional] 
**TechnicalContactId** | **string** |  | [optional] 
**TransferDate** | [**time.Time**](time.Time.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


