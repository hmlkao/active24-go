# \ServersVirtualsSSLApi

All URIs are relative to *http://api.active24.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GenerateCsrUsingPOST**](ServersVirtualsSSLApi.md#GenerateCsrUsingPOST) | **Post** /servers/virtuals/{domain}/csr/v1 | Generates new certificate signing request for specific server identified by domain.



## GenerateCsrUsingPOST

> CertificateSigningRequest GenerateCsrUsingPOST(ctx, authorization, domain, data, optional)

Generates new certificate signing request for specific server identified by domain.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**data** | [**CertificateSigningRequestData**](CertificateSigningRequestData.md)| data | 
 **optional** | ***GenerateCsrUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GenerateCsrUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

[**CertificateSigningRequest**](CertificateSigningRequest.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

