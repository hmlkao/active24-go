# CertificateSigningRequestData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**City** | **string** |  | [optional] 
**CommonName** | **string** |  | [optional] 
**Country** | **string** |  | [optional] 
**Email** | **string** |  | [optional] 
**KeyLength** | **string** |  | [optional] 
**Organization** | **string** |  | [optional] 
**OrganizationUnit** | **string** |  | [optional] 
**State** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


