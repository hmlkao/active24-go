# \EmailsA24EmailsApi

All URIs are relative to *http://api.active24.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ActivatePlusUsingPUT**](EmailsA24EmailsApi.md#ActivatePlusUsingPUT) | **Put** /emails/a24email/{emailDomain}/users/{user}/plus/v1 | Activates or deactivates A24EmailPlusData mailbox.
[**AddDomainAliasUsingPOST**](EmailsA24EmailsApi.md#AddDomainAliasUsingPOST) | **Post** /emails/a24email/{emailDomain}/aliases/v1 | Creates a domain alias.
[**AddMailboxAliasUsingPOST**](EmailsA24EmailsApi.md#AddMailboxAliasUsingPOST) | **Post** /emails/a24email/{emailDomain}/users/{user}/aliases/v1 | Adds mailbox alias.
[**AddMailboxNotifyUsingPOST**](EmailsA24EmailsApi.md#AddMailboxNotifyUsingPOST) | **Post** /emails/a24email/{emailDomain}/users/{user}/notifications/v1 | Adds recipient of mailbox notifications.
[**ChangeGraylistStatusUsingPUT**](EmailsA24EmailsApi.md#ChangeGraylistStatusUsingPUT) | **Put** /emails/a24email/{emailDomain}/users/{user}/greylist/v1 | Enables or disables greylist on email.
[**ChangeLocalDeliveryStatusUsingPUT**](EmailsA24EmailsApi.md#ChangeLocalDeliveryStatusUsingPUT) | **Put** /emails/a24email/{emailDomain}/users/{user}/local-delivery/v1 | Enables or disables local delivery on email.
[**ChangeMailboxNotifyFreqUsingPUT**](EmailsA24EmailsApi.md#ChangeMailboxNotifyFreqUsingPUT) | **Put** /emails/a24email/{emailDomain}/users/{user}/notifications/frequency/v1 | Sets frequency of notifications for mailbox.
[**CreateMailboxUsingPOST**](EmailsA24EmailsApi.md#CreateMailboxUsingPOST) | **Post** /emails/a24email/{emailDomain}/mailboxes/v1 | Creates new mailbox on ACTIVE 24 E-mail service.
[**DeleteDomainAliasUsingDELETE**](EmailsA24EmailsApi.md#DeleteDomainAliasUsingDELETE) | **Delete** /emails/a24email/{emailDomain}/aliases/{alias}/v1 | Removes email domain alias.
[**DeleteDomainCatchallUsingDELETE**](EmailsA24EmailsApi.md#DeleteDomainCatchallUsingDELETE) | **Delete** /emails/a24email/{emailDomain}/catchall/v1 | Unsets catchall.
[**DeleteMailboxAliasUsingDELETE**](EmailsA24EmailsApi.md#DeleteMailboxAliasUsingDELETE) | **Delete** /emails/a24email/{emailDomain}/users/{user}/aliases/{alias}/v1 | Removes mailbox alias.
[**DeleteMailboxNotifyUsingDELETE**](EmailsA24EmailsApi.md#DeleteMailboxNotifyUsingDELETE) | **Delete** /emails/a24email/{emailDomain}/users/{user}/notifications/{email}/v1 | Removes recipient of mailbox notifications.
[**DeleteMailboxUsingDELETE**](EmailsA24EmailsApi.md#DeleteMailboxUsingDELETE) | **Delete** /emails/a24email/{emailDomain}/users/{user}/v1 | Deletes mailbox of Active24 email.
[**EditMailboxPasswordUsingPUT**](EmailsA24EmailsApi.md#EditMailboxPasswordUsingPUT) | **Put** /emails/a24email/{emailDomain}/users/{user}/change-password/v1 | Changes mailbox password.
[**GetA24EmailBoxesUsingGET**](EmailsA24EmailsApi.md#GetA24EmailBoxesUsingGET) | **Get** /emails/a24email/{emailDomain}/mailboxes/v1 | Returns list of mailboxes on domain.
[**GetA24EmailDataUsingGET**](EmailsA24EmailsApi.md#GetA24EmailDataUsingGET) | **Get** /emails/a24email/{emailDomain}/v1 | Returns detail of ACTIVE 24 E-mail service for domain name.
[**GetA24EmailDetailDataUsingGET**](EmailsA24EmailsApi.md#GetA24EmailDetailDataUsingGET) | **Get** /emails/a24email/{emailDomain}/users/{user}/v1 | Returns detail of mailbox from ACTIVE 24 E-mail service.
[**MailboxRenameUsingPOST**](EmailsA24EmailsApi.md#MailboxRenameUsingPOST) | **Post** /emails/a24email/{emailDomain}/users/{user}/v1 | Renames mailbox.
[**SetDomainCatchallUsingPUT**](EmailsA24EmailsApi.md#SetDomainCatchallUsingPUT) | **Put** /emails/a24email/{emailDomain}/catchall/v1 | Sets domain catchall.
[**SetPostmasterUsingPUT**](EmailsA24EmailsApi.md#SetPostmasterUsingPUT) | **Put** /emails/a24email/{emailDomain}/postmaster/{user}/v1 | Sets user to be postmaster of domain.



## ActivatePlusUsingPUT

> ActivatePlusUsingPUT(ctx, authorization, emailDomain, user, plus, optional)

Activates or deactivates A24EmailPlusData mailbox.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
**plus** | [**A24EmailPlusData**](A24EmailPlusData.md)| plus | 
 **optional** | ***ActivatePlusUsingPUTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a ActivatePlusUsingPUTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddDomainAliasUsingPOST

> string AddDomainAliasUsingPOST(ctx, authorization, emailDomain, newAlias)

Creates a domain alias.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**newAlias** | [**EmailDomainAlias**](EmailDomainAlias.md)| newAlias | 

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddMailboxAliasUsingPOST

> AddMailboxAliasUsingPOST(ctx, authorization, emailDomain, user, newAlias)

Adds mailbox alias.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
**newAlias** | [**MailboxAliasData**](MailboxAliasData.md)| newAlias | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddMailboxNotifyUsingPOST

> AddMailboxNotifyUsingPOST(ctx, authorization, emailDomain, user, recipient)

Adds recipient of mailbox notifications.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
**recipient** | [**MailboxNotificationData**](MailboxNotificationData.md)| recipient | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ChangeGraylistStatusUsingPUT

> ChangeGraylistStatusUsingPUT(ctx, authorization, emailDomain, user, status)

Enables or disables greylist on email.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
**status** | [**GreylistStatus**](GreylistStatus.md)| status | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ChangeLocalDeliveryStatusUsingPUT

> ChangeLocalDeliveryStatusUsingPUT(ctx, authorization, emailDomain, user, status)

Enables or disables local delivery on email.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
**status** | [**LocalDeliveryStatusData**](LocalDeliveryStatusData.md)| status | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ChangeMailboxNotifyFreqUsingPUT

> ChangeMailboxNotifyFreqUsingPUT(ctx, authorization, emailDomain, user, freq)

Sets frequency of notifications for mailbox.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
**freq** | [**NotificationFrequencyData**](NotificationFrequencyData.md)| freq | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateMailboxUsingPOST

> CreateMailboxUsingPOST(ctx, authorization, emailDomain, formData)

Creates new mailbox on ACTIVE 24 E-mail service.

Mailbox name has to be entered without @ and domain name - for example just 'info'<br><b>Password requirements:</b> <br>at least 10 characters<br>at least 1 Uppercase character<br>at least 1 Lowercase character<br>at least 1 number

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**formData** | [**A24EmailCreateMailboxData**](A24EmailCreateMailboxData.md)| formData | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteDomainAliasUsingDELETE

> DeleteDomainAliasUsingDELETE(ctx, authorization, alias, emailDomain, optional)

Removes email domain alias.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**alias** | **string**| alias | 
**emailDomain** | **string**| emailDomain | 
 **optional** | ***DeleteDomainAliasUsingDELETEOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a DeleteDomainAliasUsingDELETEOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteDomainCatchallUsingDELETE

> DeleteDomainCatchallUsingDELETE(ctx, authorization, emailDomain)

Unsets catchall.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteMailboxAliasUsingDELETE

> DeleteMailboxAliasUsingDELETE(ctx, authorization, alias, emailDomain, user)

Removes mailbox alias.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**alias** | **string**| alias | 
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteMailboxNotifyUsingDELETE

> DeleteMailboxNotifyUsingDELETE(ctx, authorization, email, emailDomain, user)

Removes recipient of mailbox notifications.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**email** | **string**| email | 
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteMailboxUsingDELETE

> DeleteMailboxUsingDELETE(ctx, authorization, emailDomain, user, optional)

Deletes mailbox of Active24 email.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
 **optional** | ***DeleteMailboxUsingDELETEOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a DeleteMailboxUsingDELETEOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## EditMailboxPasswordUsingPUT

> EditMailboxPasswordUsingPUT(ctx, authorization, emailDomain, user, newPassword, optional)

Changes mailbox password.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
**newPassword** | **string**| newPassword | 
 **optional** | ***EditMailboxPasswordUsingPUTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a EditMailboxPasswordUsingPUTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetA24EmailBoxesUsingGET

> A24EmailBox GetA24EmailBoxesUsingGET(ctx, authorization, emailDomain)

Returns list of mailboxes on domain.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 

### Return type

[**A24EmailBox**](A24EmailBox.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetA24EmailDataUsingGET

> EmailDomainDetailData GetA24EmailDataUsingGET(ctx, authorization, emailDomain, optional)

Returns detail of ACTIVE 24 E-mail service for domain name.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
 **optional** | ***GetA24EmailDataUsingGETOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetA24EmailDataUsingGETOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **name** | **optional.String**|  | 

### Return type

[**EmailDomainDetailData**](EmailDomainDetailData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetA24EmailDetailDataUsingGET

> A24MailboxData GetA24EmailDetailDataUsingGET(ctx, authorization, emailDomain, user)

Returns detail of mailbox from ACTIVE 24 E-mail service.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 

### Return type

[**A24MailboxData**](A24MailboxData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## MailboxRenameUsingPOST

> string MailboxRenameUsingPOST(ctx, authorization, emailDomain, user, newMailboxName, optional)

Renames mailbox.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
**newMailboxName** | **string**| newMailboxName | 
 **optional** | ***MailboxRenameUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a MailboxRenameUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **name** | **optional.String**|  | 

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SetDomainCatchallUsingPUT

> SetDomainCatchallUsingPUT(ctx, authorization, emailDomain, user, optional)

Sets domain catchall.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | [**MailboxCatchallUserData**](MailboxCatchallUserData.md)| user | 
 **optional** | ***SetDomainCatchallUsingPUTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a SetDomainCatchallUsingPUTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SetPostmasterUsingPUT

> SetPostmasterUsingPUT(ctx, authorization, emailDomain, user, optional)

Sets user to be postmaster of domain.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
 **optional** | ***SetPostmasterUsingPUTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a SetPostmasterUsingPUTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

