# AutomatedDomainConfigurationFieldData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Error** | **string** |  | [optional] 
**Name** | **string** |  | [optional] 
**Order** | **int32** |  | [optional] 
**Required** | **bool** |  | [optional] 
**Type** | **string** |  | [optional] 
**ValidationRegex** | **string** |  | [optional] 
**Value** | [**map[string]interface{}**](.md) |  | [optional] 
**WriteOnly** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


