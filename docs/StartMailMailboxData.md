# StartMailMailboxData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Aliases** | **[]string** |  | [optional] 
**Autoreply** | [**AccountAutoreplyData**](AccountAutoreplyData.md) |  | [optional] 
**Forwards** | **[]string** |  | [optional] 
**GreylistEnabled** | **bool** |  | [optional] 
**LocalDeliveryEnabled** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


