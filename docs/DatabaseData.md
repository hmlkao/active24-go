# DatabaseData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DbType** | **string** |  | [optional] 
**Dbname** | **string** |  | [optional] 
**Hostname** | **string** |  | [optional] 
**QuotaLimit** | **int32** |  | [optional] 
**QuotaUsed** | **int32** |  | [optional] 
**User** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


