# RestPaymentItemData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Currency** | **string** |  | [optional] 
**DetailKey** | **string** |  | [optional] 
**Invoice** | [**PaymentInvoiceData**](PaymentInvoiceData.md) |  | [optional] 
**IssueDate** | [**time.Time**](time.Time.md) |  | [optional] 
**PayUrl** | **string** |  | [optional] 
**Price** | **float64** |  | [optional] 
**Reminders** | [**[]PaymentReminderData**](PaymentReminderData.md) |  | [optional] 
**Request** | [**PaymentRequestData**](PaymentRequestData.md) |  | [optional] 
**Status** | **string** |  | [optional] 
**Subject** | **[]string** |  | [optional] 
**SubjectConcated** | **string** |  | [optional] 
**VariableSymbol** | **string** |  | [optional] 
**VariableSymbolId** | **int64** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


