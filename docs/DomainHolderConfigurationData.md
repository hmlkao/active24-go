# DomainHolderConfigurationData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Fields** | [**[]AutomatedDomainConfigurationFieldData**](AutomatedDomainConfigurationFieldData.md) |  | [optional] 
**ProcessState** | **string** |  | [optional] 
**Updatable** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


