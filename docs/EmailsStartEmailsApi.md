# \EmailsStartEmailsApi

All URIs are relative to *http://api.active24.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddDomainAliasUsingPOST1**](EmailsStartEmailsApi.md#AddDomainAliasUsingPOST1) | **Post** /email/startmail/{emailDomain}/aliases/v1 | Creates start mail domain alias.
[**AddMailboxAliasUsingPOST1**](EmailsStartEmailsApi.md#AddMailboxAliasUsingPOST1) | **Post** /email/startmail/{emailDomain}/users/{user}/aliases/v1 | Creates start mail mailbox alias.
[**AddMailboxBlacklistUsingPOST**](EmailsStartEmailsApi.md#AddMailboxBlacklistUsingPOST) | **Post** /email/startmail/{emailDomain}/users/{user}/blacklist/v1 | Adds email to blacklist.
[**AddMailboxForwardUsingPOST**](EmailsStartEmailsApi.md#AddMailboxForwardUsingPOST) | **Post** /email/startmail/{emailDomain}/users/{user}/forwards/v1 | Adds email to the list of emails that incoming mailbox messages are forwarded to.
[**AddMailboxNotifyUsingPOST1**](EmailsStartEmailsApi.md#AddMailboxNotifyUsingPOST1) | **Post** /email/startmail/{emailDomain}/users/{user}/notifications/v1 | Adds email to notifications.
[**AddMailboxWhitelistUsingPOST**](EmailsStartEmailsApi.md#AddMailboxWhitelistUsingPOST) | **Post** /email/startmail/{emailDomain}/users/{user}/whitelist/v1 | Adds email to whitelist.
[**BlockMailboxPasswordUsingDELETE**](EmailsStartEmailsApi.md#BlockMailboxPasswordUsingDELETE) | **Delete** /email/startmail/{emailDomain}/users/{user}/password/v1 | Blocks mailbox password.
[**ChangeLocalDeliveryUsingPUT**](EmailsStartEmailsApi.md#ChangeLocalDeliveryUsingPUT) | **Put** /email/startmail/{emailDomain}/users/{user}/change-delivery/v1 | Change local delivery at mailbox.
[**ConfigureAntispamUsingPUT**](EmailsStartEmailsApi.md#ConfigureAntispamUsingPUT) | **Put** /email/startmail/{emailDomain}/users/{user}/antispam/v1 | Configures spam filtering.
[**CreateMailboxUsingPOST1**](EmailsStartEmailsApi.md#CreateMailboxUsingPOST1) | **Post** /email/startmail/{emailDomain}/mailboxes/v1 | Creates mailbox.
[**DeleteDomainAliasUsingDELETE1**](EmailsStartEmailsApi.md#DeleteDomainAliasUsingDELETE1) | **Delete** /email/startmail/{emailDomain}/aliases/{alias}/v1 | Removes start mail domain alias.
[**DeleteDomainCatchallUsingDELETE1**](EmailsStartEmailsApi.md#DeleteDomainCatchallUsingDELETE1) | **Delete** /email/startmail/{emailDomain}/catchall/v1 | Unsets domain catchall.
[**DeleteMailboxAliasUsingDELETE1**](EmailsStartEmailsApi.md#DeleteMailboxAliasUsingDELETE1) | **Delete** /email/startmail/{emailDomain}/users/{user}/aliases/{alias}/v1 | Removes start mail mailbox alias.
[**DeleteMailboxUsingDELETE1**](EmailsStartEmailsApi.md#DeleteMailboxUsingDELETE1) | **Delete** /email/startmail/{emailDomain}/users/{user}/v1 | Deletes mailbox.
[**FilterUsingPUT**](EmailsStartEmailsApi.md#FilterUsingPUT) | **Put** /email/startmail/{emailDomain}/users/{user}/filter/v1 | Enables or disables email filter required for creating filter rules.
[**GetBlackListsUsingGET**](EmailsStartEmailsApi.md#GetBlackListsUsingGET) | **Get** /email/startmail/{emailDomain}/users/{user}/blacklist/v1 | Return blacklisted emails.
[**GetStartmailDetailDataUsingGET**](EmailsStartEmailsApi.md#GetStartmailDetailDataUsingGET) | **Get** /email/startmail/{emailDomain}/users/{user}/v1 | Returns detail of start mail mailbox.
[**GetStartmailDomainDataUsingGET**](EmailsStartEmailsApi.md#GetStartmailDomainDataUsingGET) | **Get** /email/startmail/{emailDomain}/v1 | Returns detail of Start mail domain.
[**GetStartmailMailboxWarningUsingGET**](EmailsStartEmailsApi.md#GetStartmailMailboxWarningUsingGET) | **Get** /email/startmail/{emailDomain}/users/{user}/quota-warning/v1 | Returns quota warning settings.
[**GetStartmailMailboxesUsingGET**](EmailsStartEmailsApi.md#GetStartmailMailboxesUsingGET) | **Get** /email/startmail/{emailDomain}/mailboxes/v1 | Returns list of mailboxes.
[**GetWhiteListsUsingGET**](EmailsStartEmailsApi.md#GetWhiteListsUsingGET) | **Get** /email/startmail/{emailDomain}/users/{user}/whitelist/v1 | Returns whitelisted emails.
[**RemoveMailboxBlacklistUsingDELETE**](EmailsStartEmailsApi.md#RemoveMailboxBlacklistUsingDELETE) | **Delete** /email/startmail/{emailDomain}/users/{user}/blacklist/{email}/v1 | Removes email from blacklist.
[**RemoveMailboxForwardUsingDELETE**](EmailsStartEmailsApi.md#RemoveMailboxForwardUsingDELETE) | **Delete** /email/startmail/{emailDomain}/users/{user}/forwards/{mail}/v1 | Removes email from the list of emails that incoming mailbox messages are forwarded to.
[**RemoveMailboxNotifyUsingDELETE**](EmailsStartEmailsApi.md#RemoveMailboxNotifyUsingDELETE) | **Delete** /email/startmail/{emailDomain}/users/{user}/notifications/{email}/v1 | Removes email from notifications.
[**RemoveMailboxWhitelistUsingDELETE**](EmailsStartEmailsApi.md#RemoveMailboxWhitelistUsingDELETE) | **Delete** /email/startmail/{emailDomain}/users/{user}/whitelist/{email}/v1 | Removes email from whitelist.
[**SetAntispamStatusUsingPUT**](EmailsStartEmailsApi.md#SetAntispamStatusUsingPUT) | **Put** /email/startmail/{emailDomain}/users/{user}/antispam/status/v1 | Enables or disables spam filtering.
[**SetDomainCatchallUsingPUT1**](EmailsStartEmailsApi.md#SetDomainCatchallUsingPUT1) | **Put** /email/startmail/{emailDomain}/catchall/v1 | Sets domain catchall.
[**SetGreylisStatusUsingPUT**](EmailsStartEmailsApi.md#SetGreylisStatusUsingPUT) | **Put** /email/startmail/{emailDomain}/users/{user}/greylist/v1 | Enables or disables greylist on email.
[**SetMailboxUserPasswordUsingPUT**](EmailsStartEmailsApi.md#SetMailboxUserPasswordUsingPUT) | **Put** /email/startmail/{emailDomain}/users/{user}/password/v1 | Changes mailbox password.
[**SetQuotaUsingPUT**](EmailsStartEmailsApi.md#SetQuotaUsingPUT) | **Put** /email/startmail/{emailDomain}/users/{user}/size/v1 | Changes mailbox size.
[**SetStartmailMailboxWarningUsingPUT**](EmailsStartEmailsApi.md#SetStartmailMailboxWarningUsingPUT) | **Put** /email/startmail/{emailDomain}/users/{user}/quota-warning/v1 | Configures quota warning settings.



## AddDomainAliasUsingPOST1

> AddDomainAliasUsingPOST1(ctx, authorization, emailDomain, emailDomainAlias)

Creates start mail domain alias.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**emailDomainAlias** | [**EmailDomainAlias**](EmailDomainAlias.md)| emailDomainAlias | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddMailboxAliasUsingPOST1

> AddMailboxAliasUsingPOST1(ctx, authorization, emailDomain, user, mailboxAlias, optional)

Creates start mail mailbox alias.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
**mailboxAlias** | [**MailboxAliasData**](MailboxAliasData.md)| mailboxAlias | 
 **optional** | ***AddMailboxAliasUsingPOST1Opts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a AddMailboxAliasUsingPOST1Opts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddMailboxBlacklistUsingPOST

> AddMailboxBlacklistUsingPOST(ctx, authorization, emailDomain, user, email, optional)

Adds email to blacklist.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
**email** | [**Email**](Email.md)| email | 
 **optional** | ***AddMailboxBlacklistUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a AddMailboxBlacklistUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddMailboxForwardUsingPOST

> AddMailboxForwardUsingPOST(ctx, authorization, emailDomain, user, email, optional)

Adds email to the list of emails that incoming mailbox messages are forwarded to.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
**email** | [**Email**](Email.md)| email | 
 **optional** | ***AddMailboxForwardUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a AddMailboxForwardUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddMailboxNotifyUsingPOST1

> AddMailboxNotifyUsingPOST1(ctx, authorization, emailDomain, user, email, optional)

Adds email to notifications.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
**email** | [**Email**](Email.md)| email | 
 **optional** | ***AddMailboxNotifyUsingPOST1Opts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a AddMailboxNotifyUsingPOST1Opts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddMailboxWhitelistUsingPOST

> AddMailboxWhitelistUsingPOST(ctx, authorization, emailDomain, user, email, optional)

Adds email to whitelist.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
**email** | [**Email**](Email.md)| email | 
 **optional** | ***AddMailboxWhitelistUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a AddMailboxWhitelistUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## BlockMailboxPasswordUsingDELETE

> BlockMailboxPasswordUsingDELETE(ctx, authorization, emailDomain, user, optional)

Blocks mailbox password.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
 **optional** | ***BlockMailboxPasswordUsingDELETEOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a BlockMailboxPasswordUsingDELETEOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ChangeLocalDeliveryUsingPUT

> ChangeLocalDeliveryUsingPUT(ctx, authorization, emailDomain, user, blockAccount, optional)

Change local delivery at mailbox.

User has to be entered without @ and domain name - for example just 'postmaster'<br>If local delivery is deactivated, no emails will be stored in mailbox.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
**blockAccount** | [**BlockAccount**](BlockAccount.md)| blockAccount | 
 **optional** | ***ChangeLocalDeliveryUsingPUTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a ChangeLocalDeliveryUsingPUTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ConfigureAntispamUsingPUT

> ConfigureAntispamUsingPUT(ctx, authorization, emailDomain, user, antispamConfiguration, optional)

Configures spam filtering.

User has to be entered without @ and domain name - for example just 'postmaster'<br>If folder value has not been filled (or is empty), emails marked as SPAM will not be stored.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
**antispamConfiguration** | [**AntispamConfiguration**](AntispamConfiguration.md)| antispamConfiguration | 
 **optional** | ***ConfigureAntispamUsingPUTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a ConfigureAntispamUsingPUTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CreateMailboxUsingPOST1

> CreateMailboxUsingPOST1(ctx, authorization, emailDomain, formData)

Creates mailbox.

Mailbox name has to be entered without @ and domain name - for example just 'info'<br><b>Password requirements:</b> <br>at least 10 characters<br>at least 1 Uppercase character<br>at least 1 Lowercase character<br>at least 1 number

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**formData** | [**EmailPostfixCreateMailboxData**](EmailPostfixCreateMailboxData.md)| formData | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteDomainAliasUsingDELETE1

> DeleteDomainAliasUsingDELETE1(ctx, authorization, alias, emailDomain, optional)

Removes start mail domain alias.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**alias** | **string**| alias | 
**emailDomain** | **string**| emailDomain | 
 **optional** | ***DeleteDomainAliasUsingDELETE1Opts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a DeleteDomainAliasUsingDELETE1Opts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteDomainCatchallUsingDELETE1

> DeleteDomainCatchallUsingDELETE1(ctx, authorization, emailDomain, optional)

Unsets domain catchall.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
 **optional** | ***DeleteDomainCatchallUsingDELETE1Opts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a DeleteDomainCatchallUsingDELETE1Opts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteMailboxAliasUsingDELETE1

> DeleteMailboxAliasUsingDELETE1(ctx, authorization, alias, emailDomain, user, optional)

Removes start mail mailbox alias.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**alias** | **string**| alias | 
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
 **optional** | ***DeleteMailboxAliasUsingDELETE1Opts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a DeleteMailboxAliasUsingDELETE1Opts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteMailboxUsingDELETE1

> DeleteMailboxUsingDELETE1(ctx, authorization, emailDomain, user)

Deletes mailbox.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## FilterUsingPUT

> FilterUsingPUT(ctx, authorization, emailDomain, user, emailFilter)

Enables or disables email filter required for creating filter rules.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
**emailFilter** | [**EmailFilter**](EmailFilter.md)| emailFilter | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetBlackListsUsingGET

> []string GetBlackListsUsingGET(ctx, authorization, emailDomain, user, optional)

Return blacklisted emails.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
 **optional** | ***GetBlackListsUsingGETOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetBlackListsUsingGETOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

**[]string**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetStartmailDetailDataUsingGET

> A24MailboxData GetStartmailDetailDataUsingGET(ctx, authorization, emailDomain, user, optional)

Returns detail of start mail mailbox.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
 **optional** | ***GetStartmailDetailDataUsingGETOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetStartmailDetailDataUsingGETOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

[**A24MailboxData**](A24MailboxData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetStartmailDomainDataUsingGET

> EmailDomainDetailData GetStartmailDomainDataUsingGET(ctx, authorization, emailDomain, optional)

Returns detail of Start mail domain.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
 **optional** | ***GetStartmailDomainDataUsingGETOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetStartmailDomainDataUsingGETOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **name** | **optional.String**|  | 

### Return type

[**EmailDomainDetailData**](EmailDomainDetailData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetStartmailMailboxWarningUsingGET

> AccountQuotaData GetStartmailMailboxWarningUsingGET(ctx, authorization, emailDomain, user, optional)

Returns quota warning settings.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
 **optional** | ***GetStartmailMailboxWarningUsingGETOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetStartmailMailboxWarningUsingGETOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

[**AccountQuotaData**](AccountQuotaData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetStartmailMailboxesUsingGET

> []A24EmailBox GetStartmailMailboxesUsingGET(ctx, authorization, emailDomain, optional)

Returns list of mailboxes.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
 **optional** | ***GetStartmailMailboxesUsingGETOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetStartmailMailboxesUsingGETOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **name** | **optional.String**|  | 

### Return type

[**[]A24EmailBox**](A24EmailBox.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetWhiteListsUsingGET

> []string GetWhiteListsUsingGET(ctx, authorization, emailDomain, user, optional)

Returns whitelisted emails.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
 **optional** | ***GetWhiteListsUsingGETOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetWhiteListsUsingGETOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

**[]string**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## RemoveMailboxBlacklistUsingDELETE

> RemoveMailboxBlacklistUsingDELETE(ctx, authorization, email, emailDomain, user, optional)

Removes email from blacklist.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**email** | **string**| email | 
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
 **optional** | ***RemoveMailboxBlacklistUsingDELETEOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a RemoveMailboxBlacklistUsingDELETEOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## RemoveMailboxForwardUsingDELETE

> RemoveMailboxForwardUsingDELETE(ctx, authorization, emailDomain, mail, user, optional)

Removes email from the list of emails that incoming mailbox messages are forwarded to.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**mail** | **string**| mail | 
**user** | **string**| user | 
 **optional** | ***RemoveMailboxForwardUsingDELETEOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a RemoveMailboxForwardUsingDELETEOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## RemoveMailboxNotifyUsingDELETE

> RemoveMailboxNotifyUsingDELETE(ctx, authorization, email, emailDomain, user, optional)

Removes email from notifications.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**email** | **string**| email | 
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
 **optional** | ***RemoveMailboxNotifyUsingDELETEOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a RemoveMailboxNotifyUsingDELETEOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## RemoveMailboxWhitelistUsingDELETE

> RemoveMailboxWhitelistUsingDELETE(ctx, authorization, email, emailDomain, user, optional)

Removes email from whitelist.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**email** | **string**| email | 
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
 **optional** | ***RemoveMailboxWhitelistUsingDELETEOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a RemoveMailboxWhitelistUsingDELETEOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SetAntispamStatusUsingPUT

> SetAntispamStatusUsingPUT(ctx, authorization, emailDomain, user, antispamData, optional)

Enables or disables spam filtering.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
**antispamData** | [**AntispamStatus**](AntispamStatus.md)| antispamData | 
 **optional** | ***SetAntispamStatusUsingPUTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a SetAntispamStatusUsingPUTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SetDomainCatchallUsingPUT1

> SetDomainCatchallUsingPUT1(ctx, authorization, emailDomain, user, optional)

Sets domain catchall.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | [**MailboxCatchallUserData**](MailboxCatchallUserData.md)| user | 
 **optional** | ***SetDomainCatchallUsingPUT1Opts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a SetDomainCatchallUsingPUT1Opts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SetGreylisStatusUsingPUT

> SetGreylisStatusUsingPUT(ctx, authorization, emailDomain, user, status)

Enables or disables greylist on email.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
**status** | [**GreylistStatus**](GreylistStatus.md)| status | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SetMailboxUserPasswordUsingPUT

> SetMailboxUserPasswordUsingPUT(ctx, authorization, emailDomain, user, password, optional)

Changes mailbox password.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
**password** | [**MailboxPasswordData**](MailboxPasswordData.md)| password | 
 **optional** | ***SetMailboxUserPasswordUsingPUTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a SetMailboxUserPasswordUsingPUTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SetQuotaUsingPUT

> SetQuotaUsingPUT(ctx, authorization, emailDomain, user, quota, optional)

Changes mailbox size.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
**quota** | [**MailboxQuotaData**](MailboxQuotaData.md)| quota | 
 **optional** | ***SetQuotaUsingPUTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a SetQuotaUsingPUTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SetStartmailMailboxWarningUsingPUT

> SetStartmailMailboxWarningUsingPUT(ctx, authorization, emailDomain, user, accountQuotaData, optional)

Configures quota warning settings.

User has to be entered without @ and domain name - for example just 'postmaster'

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**emailDomain** | **string**| emailDomain | 
**user** | **string**| user | 
**accountQuotaData** | [**AccountQuotaData**](AccountQuotaData.md)| accountQuotaData | 
 **optional** | ***SetStartmailMailboxWarningUsingPUTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a SetStartmailMailboxWarningUsingPUTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

