# WinFtpUserFrontendData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**HomeDir** | **string** |  | [optional] 
**Id** | **int32** |  | [optional] 
**Username** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


