# DnsRecordNsUpdate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**HashId** | **string** |  | [optional] 
**Name** | **string** | Name of the record. | [optional] 
**NameServer** | **string** |  | [optional] 
**Ttl** | **int32** | Time to live. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


