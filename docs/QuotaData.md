# QuotaData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Db** | [**Quota**](Quota.md) |  | [optional] 
**Emails** | **[]string** |  | [optional] 
**Files** | [**Quota**](Quota.md) |  | [optional] 
**Mail** | [**Quota**](Quota.md) |  | [optional] 
**Max** | **int32** |  | [optional] 
**Period** | **int32** |  | [optional] 
**Used** | **int32** |  | [optional] 
**Web** | [**Quota**](Quota.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


