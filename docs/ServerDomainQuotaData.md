# ServerDomainQuotaData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DataQuota** | [**QuotaDetailData**](QuotaDetailData.md) |  | [optional] 
**Domain** | **string** |  | [optional] 
**EmailQuota** | [**QuotaDetailData**](QuotaDetailData.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


