# CreateMsWindowsVirtualParams

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DatabaseType** | **string** |  | [optional] 
**DiskSpace** | **int64** |  | [optional] 
**Domain** | **string** |  | [optional] 
**MailSpace** | **int64** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


