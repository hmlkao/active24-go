# VirtualServerData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Domain** | **string** |  | [optional] 
**Location** | **string** |  | [optional] 
**Os** | **string** |  | [optional] 
**PayedTo** | [**time.Time**](time.Time.md) |  | [optional] 
**Status** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


