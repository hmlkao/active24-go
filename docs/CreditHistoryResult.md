# CreditHistoryResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BalanceAfter** | **float32** |  | [optional] 
**BalanceBefore** | **float32** |  | [optional] 
**Currency** | **string** |  | [optional] 
**Items** | [**[]CreditHistoryItem**](CreditHistoryItem.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


