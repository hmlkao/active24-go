# DomainCzContactDiscloseData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**HiddenEmail** | **bool** |  | [optional] 
**HiddenIdentification** | **bool** |  | [optional] 
**HiddenNotificationEmail** | **bool** |  | [optional] 
**HiddenPhone** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


