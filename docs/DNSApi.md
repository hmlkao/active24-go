# \DNSApi

All URIs are relative to *http://api.active24.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddNewARecordUsingPOST**](DNSApi.md#AddNewARecordUsingPOST) | **Post** /dns/{domain}/a/v1 | Creates a new A record.
[**AddNewAaaaRecordUsingPOST**](DNSApi.md#AddNewAaaaRecordUsingPOST) | **Post** /dns/{domain}/aaaa/v1 | Creates a new AAAA record.
[**AddNewCaaRecordUsingPOST**](DNSApi.md#AddNewCaaRecordUsingPOST) | **Post** /dns/{domain}/caa/v1 | Creates a new CAA record.
[**AddNewCnameRecordUsingPOST**](DNSApi.md#AddNewCnameRecordUsingPOST) | **Post** /dns/{domain}/cname/v1 | Creates a new CNAME record.
[**AddNewMxRecordUsingPOST**](DNSApi.md#AddNewMxRecordUsingPOST) | **Post** /dns/{domain}/mx/v1 | Creates a new MX record.
[**AddNewNsRecordUsingPOST**](DNSApi.md#AddNewNsRecordUsingPOST) | **Post** /dns/{domain}/ns/v1 | Creates a new NS record.
[**AddNewSrvRecordUsingPOST**](DNSApi.md#AddNewSrvRecordUsingPOST) | **Post** /dns/{domain}/srv/v1 | Creates a new SRV record.
[**AddNewSshfpRecordUsingPOST**](DNSApi.md#AddNewSshfpRecordUsingPOST) | **Post** /dns/{domain}/sshfp/v1 | Creates a new SSHFP record.
[**AddNewTlsaRecordUsingPOST**](DNSApi.md#AddNewTlsaRecordUsingPOST) | **Post** /dns/{domain}/tlsa/v1 | Creates a new TLSA record.
[**AddNewTxtRecordUsingPOST**](DNSApi.md#AddNewTxtRecordUsingPOST) | **Post** /dns/{domain}/txt/v1 | Creates a new TXT record.
[**DeleteDnsRecordUsingDELETE**](DNSApi.md#DeleteDnsRecordUsingDELETE) | **Delete** /dns/{domain}/{dnsRecordHashId}/v1 | Deletes existing dns record (all types) by hashId.
[**GetDnsDomainsUsingGET**](DNSApi.md#GetDnsDomainsUsingGET) | **Get** /dns/domains/v1 | Returns a list of domains which have DNS records managed by Active 24.
[**GetDnsRecordsUsingGET**](DNSApi.md#GetDnsRecordsUsingGET) | **Get** /dns/{domain}/records/v1 | Returns a list of DNS records for the domain.
[**UpdateARecordUsingPUT**](DNSApi.md#UpdateARecordUsingPUT) | **Put** /dns/{domain}/a/v1 | Updates an existing A record.
[**UpdateAaaaRecordUsingPUT**](DNSApi.md#UpdateAaaaRecordUsingPUT) | **Put** /dns/{domain}/aaaa/v1 | Updates an existing AAAA record.
[**UpdateCaaRecordUsingPUT**](DNSApi.md#UpdateCaaRecordUsingPUT) | **Put** /dns/{domain}/caa/v1 | Updates an existing CAA record.
[**UpdateCnameRecordUsingPUT**](DNSApi.md#UpdateCnameRecordUsingPUT) | **Put** /dns/{domain}/cname/v1 | Updates an existing CNAME record.
[**UpdateMxRecordUsingPUT**](DNSApi.md#UpdateMxRecordUsingPUT) | **Put** /dns/{domain}/mx/v1 | Updates an existing MX record.
[**UpdateNsRecordUsingPUT**](DNSApi.md#UpdateNsRecordUsingPUT) | **Put** /dns/{domain}/ns/v1 | Updates an existing NS record.
[**UpdateSrvRecordUsingPUT**](DNSApi.md#UpdateSrvRecordUsingPUT) | **Put** /dns/{domain}/srv/v1 | Updates an existing SRV record.
[**UpdateSshfpRecordUsingPUT**](DNSApi.md#UpdateSshfpRecordUsingPUT) | **Put** /dns/{domain}/sshfp/v1 | Updates an existing SSHFP record.
[**UpdateTlsaRecordUsingPUT**](DNSApi.md#UpdateTlsaRecordUsingPUT) | **Put** /dns/{domain}/tlsa/v1 | Updates an existing TLSA record.
[**UpdateTxtRecordUsingPUT**](DNSApi.md#UpdateTxtRecordUsingPUT) | **Put** /dns/{domain}/txt/v1 | Updates an existing TXT record.



## AddNewARecordUsingPOST

> AddNewARecordUsingPOST(ctx, authorization, domain, newDnsRecord, optional)

Creates a new A record.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**newDnsRecord** | [**DnsRecordACreate**](DnsRecordACreate.md)| newDnsRecord | 
 **optional** | ***AddNewARecordUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a AddNewARecordUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddNewAaaaRecordUsingPOST

> AddNewAaaaRecordUsingPOST(ctx, authorization, domain, newDnsRecord, optional)

Creates a new AAAA record.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**newDnsRecord** | [**DnsRecordAaaaCreate**](DnsRecordAaaaCreate.md)| newDnsRecord | 
 **optional** | ***AddNewAaaaRecordUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a AddNewAaaaRecordUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddNewCaaRecordUsingPOST

> AddNewCaaRecordUsingPOST(ctx, authorization, domain, newDnsRecord, optional)

Creates a new CAA record.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**newDnsRecord** | [**DnsRecordCaaCreate**](DnsRecordCaaCreate.md)| newDnsRecord | 
 **optional** | ***AddNewCaaRecordUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a AddNewCaaRecordUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddNewCnameRecordUsingPOST

> AddNewCnameRecordUsingPOST(ctx, authorization, domain, newDnsRecord, optional)

Creates a new CNAME record.

If a dot hasn't been entered as the last character of alias value, the domain name will be added to the end of alias.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**newDnsRecord** | [**DnsRecordCnameCreate**](DnsRecordCnameCreate.md)| newDnsRecord | 
 **optional** | ***AddNewCnameRecordUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a AddNewCnameRecordUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddNewMxRecordUsingPOST

> AddNewMxRecordUsingPOST(ctx, authorization, domain, newDnsRecord, optional)

Creates a new MX record.

If a dot hasn't been entered as the last character of alias value, the domain name will be added to the end of alias.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**newDnsRecord** | [**DnsRecordMxCreate**](DnsRecordMxCreate.md)| newDnsRecord | 
 **optional** | ***AddNewMxRecordUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a AddNewMxRecordUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddNewNsRecordUsingPOST

> AddNewNsRecordUsingPOST(ctx, authorization, domain, newDnsRecord, optional)

Creates a new NS record.

A dot should be entered as the last character of name server value.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**newDnsRecord** | [**DnsRecordNsCreate**](DnsRecordNsCreate.md)| newDnsRecord | 
 **optional** | ***AddNewNsRecordUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a AddNewNsRecordUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddNewSrvRecordUsingPOST

> AddNewSrvRecordUsingPOST(ctx, authorization, domain, newDnsRecord, optional)

Creates a new SRV record.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**newDnsRecord** | [**DnsRecordSrvCreate**](DnsRecordSrvCreate.md)| newDnsRecord | 
 **optional** | ***AddNewSrvRecordUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a AddNewSrvRecordUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddNewSshfpRecordUsingPOST

> AddNewSshfpRecordUsingPOST(ctx, authorization, domain, newDnsRecord, optional)

Creates a new SSHFP record.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**newDnsRecord** | [**DnsRecordSshfpCreate**](DnsRecordSshfpCreate.md)| newDnsRecord | 
 **optional** | ***AddNewSshfpRecordUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a AddNewSshfpRecordUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddNewTlsaRecordUsingPOST

> AddNewTlsaRecordUsingPOST(ctx, authorization, domain, newDnsRecord, optional)

Creates a new TLSA record.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**newDnsRecord** | [**DnsRecordTlsaCreate**](DnsRecordTlsaCreate.md)| newDnsRecord | 
 **optional** | ***AddNewTlsaRecordUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a AddNewTlsaRecordUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## AddNewTxtRecordUsingPOST

> AddNewTxtRecordUsingPOST(ctx, authorization, domain, newDnsRecord, optional)

Creates a new TXT record.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**newDnsRecord** | [**DnsRecordTxtCreate**](DnsRecordTxtCreate.md)| newDnsRecord | 
 **optional** | ***AddNewTxtRecordUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a AddNewTxtRecordUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## DeleteDnsRecordUsingDELETE

> DnsRecord DeleteDnsRecordUsingDELETE(ctx, authorization, dnsRecordHashId, domain, optional)

Deletes existing dns record (all types) by hashId.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**dnsRecordHashId** | **string**| dnsRecordHashId | 
**domain** | **string**| domain | 
 **optional** | ***DeleteDnsRecordUsingDELETEOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a DeleteDnsRecordUsingDELETEOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

[**DnsRecord**](DnsRecord.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetDnsDomainsUsingGET

> []string GetDnsDomainsUsingGET(ctx, authorization)

Returns a list of domains which have DNS records managed by Active 24.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]

### Return type

**[]string**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetDnsRecordsUsingGET

> []DnsRecord GetDnsRecordsUsingGET(ctx, authorization, domain, optional)

Returns a list of DNS records for the domain.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
 **optional** | ***GetDnsRecordsUsingGETOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetDnsRecordsUsingGETOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **name** | **optional.String**|  | 

### Return type

[**[]DnsRecord**](DnsRecord.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateARecordUsingPUT

> UpdateARecordUsingPUT(ctx, authorization, domain, dnsRecord, optional)

Updates an existing A record.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**dnsRecord** | [**DnsRecordAUpdate**](DnsRecordAUpdate.md)| dnsRecord | 
 **optional** | ***UpdateARecordUsingPUTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a UpdateARecordUsingPUTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateAaaaRecordUsingPUT

> UpdateAaaaRecordUsingPUT(ctx, authorization, domain, dnsRecord, optional)

Updates an existing AAAA record.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**dnsRecord** | [**DnsRecordAaaaUpdate**](DnsRecordAaaaUpdate.md)| dnsRecord | 
 **optional** | ***UpdateAaaaRecordUsingPUTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a UpdateAaaaRecordUsingPUTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateCaaRecordUsingPUT

> UpdateCaaRecordUsingPUT(ctx, authorization, domain, dnsRecord, optional)

Updates an existing CAA record.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**dnsRecord** | [**DnsRecordCaaUpdate**](DnsRecordCaaUpdate.md)| dnsRecord | 
 **optional** | ***UpdateCaaRecordUsingPUTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a UpdateCaaRecordUsingPUTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateCnameRecordUsingPUT

> UpdateCnameRecordUsingPUT(ctx, authorization, domain, dnsRecord, optional)

Updates an existing CNAME record.

If a dot hasn't been entered as the last character of alias value, the domain name will be added to the end of alias.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**dnsRecord** | [**DnsRecordCnameUpdate**](DnsRecordCnameUpdate.md)| dnsRecord | 
 **optional** | ***UpdateCnameRecordUsingPUTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a UpdateCnameRecordUsingPUTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateMxRecordUsingPUT

> UpdateMxRecordUsingPUT(ctx, authorization, domain, dnsRecord, optional)

Updates an existing MX record.

If a dot hasn't been entered as the last character of alias value, the domain name will be added to the end of alias.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**dnsRecord** | [**DnsRecordMxUpdate**](DnsRecordMxUpdate.md)| dnsRecord | 
 **optional** | ***UpdateMxRecordUsingPUTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a UpdateMxRecordUsingPUTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateNsRecordUsingPUT

> UpdateNsRecordUsingPUT(ctx, authorization, domain, dnsRecord, optional)

Updates an existing NS record.

If a dot hasn't been entered as the last character of name server value, the ValidationException will be thrown.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**dnsRecord** | [**DnsRecordNsUpdate**](DnsRecordNsUpdate.md)| dnsRecord | 
 **optional** | ***UpdateNsRecordUsingPUTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a UpdateNsRecordUsingPUTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateSrvRecordUsingPUT

> UpdateSrvRecordUsingPUT(ctx, authorization, domain, dnsRecord, optional)

Updates an existing SRV record.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**dnsRecord** | [**DnsRecordSrvUpdate**](DnsRecordSrvUpdate.md)| dnsRecord | 
 **optional** | ***UpdateSrvRecordUsingPUTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a UpdateSrvRecordUsingPUTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateSshfpRecordUsingPUT

> UpdateSshfpRecordUsingPUT(ctx, authorization, domain, dnsRecord, optional)

Updates an existing SSHFP record.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**dnsRecord** | [**DnsRecordSshfpUpdate**](DnsRecordSshfpUpdate.md)| dnsRecord | 
 **optional** | ***UpdateSshfpRecordUsingPUTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a UpdateSshfpRecordUsingPUTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateTlsaRecordUsingPUT

> UpdateTlsaRecordUsingPUT(ctx, authorization, domain, dnsRecord, optional)

Updates an existing TLSA record.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**dnsRecord** | [**DnsRecordTlsaUpdate**](DnsRecordTlsaUpdate.md)| dnsRecord | 
 **optional** | ***UpdateTlsaRecordUsingPUTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a UpdateTlsaRecordUsingPUTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## UpdateTxtRecordUsingPUT

> UpdateTxtRecordUsingPUT(ctx, authorization, domain, dnsRecord, optional)

Updates an existing TXT record.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**domain** | **string**| domain | 
**dnsRecord** | [**DnsRecordTxtUpdate**](DnsRecordTxtUpdate.md)| dnsRecord | 
 **optional** | ***UpdateTxtRecordUsingPUTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a UpdateTxtRecordUsingPUTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

