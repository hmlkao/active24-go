# DomainRegistrationPaymentRowData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Text** | **string** |  | [optional] 
**UnitPriceWithoutVat** | **float32** |  | [optional] 
**UnitQuantity** | **int32** |  | [optional] 
**VatRate** | **float32** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


