# UnlimitedMultihostingDetailData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | [optional] 
**OperationSystem** | **string** |  | [optional] 
**VirtualServerLimit** | **int32** |  | [optional] 
**VirtualServerList** | [**[]MultihostingVirtualData**](MultihostingVirtualData.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


