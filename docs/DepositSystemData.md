# DepositSystemData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Created** | [**time.Time**](time.Time.md) |  | [optional] 
**Paid** | [**time.Time**](time.Time.md) |  | [optional] 
**Price** | **float32** |  | [optional] 
**PriceVatIncluded** | **float32** |  | [optional] 
**VariableSymbol** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


