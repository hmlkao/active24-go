# DomainInfoData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Active** | **bool** |  | [optional] 
**Aliases** | **[]string** |  | [optional] 
**Archive** | **bool** |  | [optional] 
**Backups** | **[]string** |  | [optional] 
**BilledTo** | [**time.Time**](time.Time.md) |  | [optional] 
**Block** | [**map[string]interface{}**](.md) |  | [optional] 
**Catchall** | **string** |  | [optional] 
**DefaultSpace** | **int64** |  | [optional] 
**Domain** | **string** |  | [optional] 
**Flexible** | **bool** |  | [optional] 
**FreeSpace** | **int64** |  | [optional] 
**Full** | **bool** |  | [optional] 
**Host** | **string** |  | [optional] 
**Limits** | [**DomainLimitsData**](DomainLimitsData.md) |  | [optional] 
**MaxAliasCount** | **int32** |  | [optional] 
**MaxPlusUsers** | **int32** |  | [optional] 
**MaxUsers** | **int32** |  | [optional] 
**Model** | [**map[string]interface{}**](.md) |  | [optional] 
**Name** | **string** |  | [optional] 
**Node** | **string** |  | [optional] 
**OpId** | **int32** |  | [optional] 
**Owner** | **string** |  | [optional] 
**Payer** | **string** |  | [optional] 
**Postmaster** | **string** |  | [optional] 
**Quota** | **int64** |  | [optional] 
**ServerName** | **string** |  | [optional] 
**Sid** | **int32** |  | [optional] 
**Space** | **int64** |  | [optional] 
**Status** | **string** |  | [optional] 
**Type** | **string** |  | [optional] 
**UsedSpace** | **int64** |  | [optional] 
**Users** | **[]string** |  | [optional] 
**UsersDetails** | [**[]AccountInfoData**](AccountInfoData.md) |  | [optional] 
**VisibleToCustomer** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


