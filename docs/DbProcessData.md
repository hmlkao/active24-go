# DbProcessData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DbName** | **string** |  | [optional] 
**DbUser** | **string** |  | [optional] 
**Id** | **int32** |  | [optional] 
**Ip** | **string** |  | [optional] 
**LastCommand** | **string** |  | [optional] 
**ProcessName** | **string** |  | [optional] 
**Time** | **int32** |  | [optional] 
**Type** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


