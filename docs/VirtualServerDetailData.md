# VirtualServerDetailData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Aliases** | **[]string** |  | [optional] 
**AlternativeAddress** | **string** |  | [optional] 
**Domain** | **string** |  | [optional] 
**Homedir** | **string** |  | [optional] 
**HostingType** | **string** |  | [optional] 
**Ipv4** | **string** |  | [optional] 
**Ipv6** | **string** |  | [optional] 
**Location** | **string** |  | [optional] 
**Os** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


