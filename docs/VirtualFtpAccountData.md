# VirtualFtpAccountData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CountryCodes** | **[]string** |  | [optional] 
**Directory** | **string** |  | [optional] 
**HomeDir** | **string** |  | [optional] 
**Ip** | **map[string]string** |  | [optional] 
**MainAccount** | **bool** |  | [optional] 
**UserName** | **string** |  | [optional] 
**Value** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


