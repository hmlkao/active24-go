# PaymentItemDetailData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Currency** | **string** |  | [optional] 
**InvoiceDocumentUrl** | **string** |  | [optional] 
**IssueDate** | [**time.Time**](time.Time.md) |  | [optional] 
**PayUrl** | **string** |  | [optional] 
**PaymentRequestDocumentUrl** | **string** |  | [optional] 
**Price** | **float64** |  | [optional] 
**Reminders** | [**[]PaymentReminderData**](PaymentReminderData.md) |  | [optional] 
**Status** | **string** |  | [optional] 
**Subject** | **[]string** |  | [optional] 
**VariableSymbol** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


