# DomainData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ExpirationDate** | [**time.Time**](time.Time.md) |  | [optional] 
**HolderFullName** | **string** |  | [optional] 
**Name** | **string** |  | [optional] 
**Payable** | **bool** |  | [optional] 
**PayedTo** | [**time.Time**](time.Time.md) |  | [optional] 
**Status** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


