# DnsRecordCaaUpdate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CaaValue** | **string** |  | [optional] 
**Flags** | **int32** |  | [optional] 
**HashId** | **string** |  | [optional] 
**Name** | **string** | Name of the record. | [optional] 
**Tag** | **string** |  | [optional] 
**Ttl** | **int32** | Time to live. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


