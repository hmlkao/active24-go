# \PaymentsDepositsApi

All URIs are relative to *http://api.active24.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CancelDomainRenewalUsingDELETE**](PaymentsDepositsApi.md#CancelDomainRenewalUsingDELETE) | **Delete** /payments/deposits/renewals/{invoiceId}/v1 | Cancels domain renewal.
[**GetDomainRenewalsUsingGET**](PaymentsDepositsApi.md#GetDomainRenewalsUsingGET) | **Get** /payments/deposits/renewals/v1 | Returns list of domains for renewal.
[**GetPaymentDepositOverviewUsingGET**](PaymentsDepositsApi.md#GetPaymentDepositOverviewUsingGET) | **Get** /payments/deposits/years/{year}/v1 | Returns overview of payments in deposit system for given year.
[**RenewDomainUsingPOST**](PaymentsDepositsApi.md#RenewDomainUsingPOST) | **Post** /payments/deposits/renewals/{invoiceId}/v1 | Renews domain from deposit system.



## CancelDomainRenewalUsingDELETE

> CancelDomainRenewalUsingDELETE(ctx, authorization, invoiceId, order, optional)

Cancels domain renewal.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**invoiceId** | **int32**| invoiceId | 
**order** | [**PaymentOrderData**](PaymentOrderData.md)| order | 
 **optional** | ***CancelDomainRenewalUsingDELETEOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a CancelDomainRenewalUsingDELETEOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetDomainRenewalsUsingGET

> []DepositSystemFuturePaymentData GetDomainRenewalsUsingGET(ctx, authorization, optional)

Returns list of domains for renewal.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
 **optional** | ***GetDomainRenewalsUsingGETOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetDomainRenewalsUsingGETOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **name** | **optional.String**|  | 

### Return type

[**[]DepositSystemFuturePaymentData**](DepositSystemFuturePaymentData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetPaymentDepositOverviewUsingGET

> DepositSystemRestData GetPaymentDepositOverviewUsingGET(ctx, authorization, year, optional)

Returns overview of payments in deposit system for given year.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**year** | **int32**| year | 
 **optional** | ***GetPaymentDepositOverviewUsingGETOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetPaymentDepositOverviewUsingGETOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **name** | **optional.String**|  | 

### Return type

[**DepositSystemRestData**](DepositSystemRestData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## RenewDomainUsingPOST

> DomainMaintenanceResultData RenewDomainUsingPOST(ctx, authorization, invoiceId, order, optional)

Renews domain from deposit system.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**invoiceId** | **int32**| invoiceId | 
**order** | [**PaymentOrderData**](PaymentOrderData.md)| order | 
 **optional** | ***RenewDomainUsingPOSTOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a RenewDomainUsingPOSTOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **name** | **optional.String**|  | 

### Return type

[**DomainMaintenanceResultData**](DomainMaintenanceResultData.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

