# DomainRegistrationPaymentDetailData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BankTransferData** | [**DomainRegistrationPaymentBankTransferData**](DomainRegistrationPaymentBankTransferData.md) |  | [optional] 
**Currency** | **string** |  | [optional] 
**InvoiceRows** | [**[]DomainRegistrationPaymentRowData**](DomainRegistrationPaymentRowData.md) |  | [optional] 
**Message** | **string** |  | [optional] 
**PaymentUrl** | **string** |  | [optional] 
**PriceToPay** | **float32** |  | [optional] 
**VariableSymbol** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


