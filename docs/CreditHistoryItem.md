# CreditHistoryItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Amount** | **float32** |  | [optional] 
**Currency** | **string** |  | [optional] 
**CustomerId** | **string** |  | [optional] 
**Inserted** | [**time.Time**](time.Time.md) |  | [optional] 
**Type** | **string** |  | [optional] 
**VarSymbol** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


