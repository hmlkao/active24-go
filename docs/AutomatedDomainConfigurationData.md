# AutomatedDomainConfigurationData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DomainAdminContactConfiguration** | [**AdminContactConfigurationData**](AdminContactConfigurationData.md) |  | [optional] 
**DomainHolderConfiguration** | [**DomainHolderConfigurationData**](DomainHolderConfigurationData.md) |  | [optional] 
**DomainNameserverConfiguration** | [**DomainNameserverConfigurationData**](DomainNameserverConfigurationData.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


