# \CreditSystemApi

All URIs are relative to *http://api.active24.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetCreditBalanceUsingGET**](CreditSystemApi.md#GetCreditBalanceUsingGET) | **Get** /credit-system/balance/v1 | Returns a credit balance.
[**GetHistoryUsingGET**](CreditSystemApi.md#GetHistoryUsingGET) | **Get** /credit-system/history/v1 | Returns a history of credit operations between fromDate and toDate.
[**PayVsByCreditUsingPOST**](CreditSystemApi.md#PayVsByCreditUsingPOST) | **Post** /credit-system/{variableSymbol}/pay/v1 | Pay variable symbol by credit.



## GetCreditBalanceUsingGET

> CreditSystemBalance GetCreditBalanceUsingGET(ctx, authorization)

Returns a credit balance.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]

### Return type

[**CreditSystemBalance**](CreditSystemBalance.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetHistoryUsingGET

> CreditHistoryResult GetHistoryUsingGET(ctx, authorization, optional)

Returns a history of credit operations between fromDate and toDate.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
 **optional** | ***GetHistoryUsingGETOpts** | optional parameters | nil if no parameters

### Optional Parameters

Optional parameters are passed through a pointer to a GetHistoryUsingGETOpts struct


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **fromDate** | **optional.Time**| Format of date is MM-dd-yyyy | 
 **toDate** | **optional.Time**| Format of date is MM-dd-yyyy | 

### Return type

[**CreditHistoryResult**](CreditHistoryResult.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## PayVsByCreditUsingPOST

> PayVsByCreditUsingPOST(ctx, authorization, variableSymbol)

Pay variable symbol by credit.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**variableSymbol** | **string**| variableSymbol | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

