# \AutoPaymentsApi

All URIs are relative to *http://api.active24.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**RemoveAutoPayForVarSymbolUsingDELETE**](AutoPaymentsApi.md#RemoveAutoPayForVarSymbolUsingDELETE) | **Delete** /auto-payments/{variableSymbol}/autopay/v1 | Cancels automatic payment from credit for variable symbol.



## RemoveAutoPayForVarSymbolUsingDELETE

> RemoveAutoPayForVarSymbolUsingDELETE(ctx, authorization, variableSymbol)

Cancels automatic payment from credit for variable symbol.

### Required Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**authorization** | **string**| Authorization Token | [default to Bearer &lt;Auth Token&gt;]
**variableSymbol** | **string**| variableSymbol | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

