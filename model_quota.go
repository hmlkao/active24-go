/*
 * Active24 REST API Documentation
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * API version: 1.0.0
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package a24
// Quota struct for Quota
type Quota struct {
	Limit int32 `json:"limit,omitempty"`
	Max int32 `json:"max,omitempty"`
	Percentage int32 `json:"percentage,omitempty"`
	Used int32 `json:"used,omitempty"`
}
