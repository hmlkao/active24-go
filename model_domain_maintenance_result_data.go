/*
 * Active24 REST API Documentation
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * API version: 1.0.0
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package a24
// DomainMaintenanceResultData struct for DomainMaintenanceResultData
type DomainMaintenanceResultData struct {
	Result string `json:"result,omitempty"`
}
