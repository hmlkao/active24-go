/*
 * Active24 REST API Documentation
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * API version: 1.0.0
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package a24
// PaymentInvoiceData struct for PaymentInvoiceData
type PaymentInvoiceData struct {
	CreditNote bool `json:"creditNote,omitempty"`
	DocumentUrl string `json:"documentUrl,omitempty"`
	InvoiceIdentifier string `json:"invoiceIdentifier,omitempty"`
}
